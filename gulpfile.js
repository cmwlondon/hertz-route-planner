/*
npm init
sudo npm install gulp -g
sudo npm install gulp --save-dev
sudo npm install gulp-csso gulp-sass gulp-concat gulp-uglify --save-dev
*/

var gulp = require('gulp');
var minifyCSS = require('gulp-csso');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('sass', function(){
  return gulp.src('assets/css/*.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('assets/css/'))
});

gulp.task('sass-watcher', function(){
	console.log('sass-watcher START');
	var watcher = gulp.watch('assets/css/*.scss', ['sass']);
	watcher.on('change', function(event) {
	  console.log('scss file updated');
	});
});

gulp.task('js', function(){
	gulp.src('assets/js/main/*.js')
	.pipe(concat('global.js'))
	// .pipe(uglify())
	.pipe(gulp.dest('assets/js/'))	
});

gulp.task('js-watcher', function(){
	console.log('js-watcher START');
	var watcher = gulp.watch('assets/js/main/*.js', ['js']);
	watcher.on('change', function(event) {
	  console.log('js file updated');
	});
});

gulp.task('default', ['sass-watcher','js-watcher'], function(){
});