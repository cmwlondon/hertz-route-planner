(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzPlaces
	/* ------------------------------------------------ */
	
	function HertzPlaces(parameters) {
		this.parameters = parameters;

		this.items = [];
		this.count = 0;

		this.init();
	}
	
	HertzPlaces.prototype = {
		"constructor" : HertzPlaces,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

		},

		/* --------------------------------------------------------------------------- */

		"addAllPlaces" : function (places) {
			var that = this;

			this.items = places;
			this.count = this.items.length;
		},

		/* --------------------------------------------------------------------------- */

		"addPlace" : function (place) {
			var that = this;
			
			if ( !this.isPlaceInList(place) ) {
				this.items.push(place);
				this.count = this.items.length;
			}
		},

		/* --------------------------------------------------------------------------- */

		"getPlaces" : function () {
			var that = this;
			return this.items;
		},

		/* --------------------------------------------------------------------------- */

		"getCount" : function () {
			var that = this;
			return this.count;
		},

		/* --------------------------------------------------------------------------- */

		"empty" : function () {
			var that = this;
			
			this.items = [];
			this.count = 0;
		},

		/* --------------------------------------------------------------------------- */

		"isPlaceInList" : function (place) {
			var that = this,
				placeIndex = 0,
				thisPlace;

			if ( this.count > 0) {
				do {
					thisPlace = this.items[ placeIndex ];			
					if (place.place_id === thisPlace.place_id ) {
						return true;
					}

					placeIndex++;
				} while ( placeIndex < this.count )
			}

			return false;
		},

		/* --------------------------------------------------------------------------- */

		"typeFilter" : function (requiredTypes) {
			var that = this,
				foundTypes = {},
				workingPOI = [];

			this.items.map(function (place) {
				if ( requiredTypes.indexOf(place.group) != -1) {

					workingPOI.push(place);

					// build list of categories existing in search results
					if ( !foundTypes.hasOwnProperty(place.group) ) {
						foundTypes[place.group] = 1;
					} else {
						foundTypes[place.group]++;
					}

				}

			});

			this.items = workingPOI;
			this.count = this.items.length;

			return foundTypes;
		},

		/* --------------------------------------------------------------------------- */

		"brandFilter" : function (parameters) {
			var that = this,
				workingPOI = [],
				regexString = '',
				testRegEx,
				result,
				badPlaceIndices = [];


			// arrange list so that low prestige brands 'subway', 'macdonalds' get moved to end of list
			// 'Subway', 'Pizza Hut', 'Pizza Express', 'Nando'
			// /Subway|Pizza Hut|Pizza Express|Nando/gi

			/*
			parameters.requiredTypes = ['restaraunt','spa','park']
			parameters.filters = [
				{
					"type" : "restaurant",
					"brands" : ['Subway','MacDonalds','Pizza Express','Pizza Hut']
				}
			]
			*/


			parameters.filters.map(function(filter) {

				if ( parameters.requiredTypes.indexOf(filter.type) != -1 || filter.type == 'global' ) {
				// if ( filter.type == 'global' ) {
					// filter type exists in required types, check place name against list of brands/names

					// build regex from list of brands
					filter.brands.map(function(brand){
						regexString = (regexString != '') ? regexString + '|' : regexString;
						regexString = regexString + brand
					});
					
					testRegEx = new RegExp(regexString, 'i');

					// iterate through places to see if the place name matches one of the proscribed brands
					that.items.map(function(place, placeIndex){
						result = testRegEx.exec(place.name);
						if(result != null){
							// console.log(result[0]);
							badPlaceIndices.push(placeIndex);
						}

					});
					badPlaceIndices.reverse(0); // reverse order

					// remove identified places
					if (badPlaceIndices.length > 0) {
						var rpi = 0,
							rpc = badPlaceIndices.length,
							trp;

						do {
							trp = badPlaceIndices[rpi];
							that.items.splice(trp, 1);

							rpi++;
						} while (rpi < rpc)

						that.count = that.items.length;
					}
				}
			});			
		},

		/* --------------------------------------------------------------------------- */

		"ratingSort" : function () {
			var that = this;

			// sort items by rating 

			// this.items = this.quickSort(this.items, 0, this.count -1);
		    this.items.sort(function (a, b)
		    {
		        return b.rating - a.rating; // descending order based on rating property
		    });
		}, 

		/* --------------------------------------------------------------------------- */

		"quickSort" : function  (arr, left, right) {
			var i = left,
				j = right,
				tmp,
				pivotidx = (left + right) / 2,
				pivot = arr[pivotidx.toFixed()].rating;  

			/* partition */
			while (i <= j)
			{
				// ascending order
				/*
				while (arr[i].rating < pivot) {
					i++;
				}

				while (arr[j].rating > pivot) {
					j--;
				}
				*/

				// descending order
				while (arr[i].rating > pivot) {
					i++;
				}

				while (arr[j].rating < pivot) {
					j--;
				}
				if (i <= j)
				{
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;

					i++;
					j--;
				}
			}

			/* recursion */
			if (left < j)
				this.quickSort(arr, left, j);
			if (i < right)
				this.quickSort(arr, i, right);
			return arr;
		}

		/* --------------------------------------------------------------------------- */	
	}

  
	window.HertzPlaces = HertzPlaces;

})();
(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzPlacesSidebar
	/* ------------------------------------------------ */
	
	function HertzPlacesSidebar(parameters) {
		this.parameters = parameters;

		this.language = parameters.language;

		this.list = parameters.list;
		this.itemsPerPage = parameters.itemsPerPage;
		this.itemHeight = parameters.itemHeight;
		this.pageHeight = parameters.pageHeight;
		this.controls = parameters.controls;

		this.upButton;
		this.downButton;
		this.items = [];
		this.entries;
		this.count = 0;

		this.pages = 0;
		this.currentPage = 0;
		this.listOffset = 0;

		this.isOpen;
		this.openEntryIndex = -1;
		this.enigma = 0;
		this.bounceMarkerIndex = -1;
		this.bounceMarkerId = '';
		this.imageLoaders = [];

		this.thirdSpeed = 250;

		this.init();
	}
	
	HertzPlacesSidebar.prototype = {
		"constructor" : HertzPlacesSidebar,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			this.pageoffset = this.pageHeight;

			this.controls.each(function(i){
				if ( jQuery(this).hasClass('up') ) {
					that.upButton = jQuery(this);
				} else {
					that.downButton = jQuery(this);
				}
			});

			this.controls.on('click', function (e) {
				e.preventDefault();

				if (jQuery(this).hasClass('up')) {
					that.pageUp();
				} else {
					that.pageDown();
				}
			});
		},

		/* --------------------------------------------------------------------------- */

		"configure" : function () {
			var that = this;

			this.update();

		},

		/* --------------------------------------------------------------------------- */
		
		"update" : function () {
			var that = this;

			this.entries = this.list.find('li.entry').not('li.template');
			this.count = this.entries.length;

			this.pages = Math.floor(this.count / this.itemsPerPage);
			this.pages = (this.count % this.itemsPerPage > 0) ? this.pages + 1 : this.pages;
			this.currentPage = 0;

			this.updateControls();
		},

		/* --------------------------------------------------------------------------- */
		
		"pageUp" : function () {
			var that = this;

			if ( this.currentPage > 0 ) {
				this.currentPage = this.currentPage - 1;
				this.pageJump(this.currentPage);
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"pageDown" : function () {
			var that = this;

			if ( this.currentPage < (this.pages - 1) ) {
				this.currentPage = this.currentPage + 1;
				this.pageJump(this.currentPage);
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"pageJump" : function (pageIndex) {
			var that = this;

			this.enigma = this.count % 3;

			this.updateControls();

			this.listOffset = this.pageoffset * -pageIndex;

			if ( pageIndex == (this.pages - 1) && this.pages > 1 ) {
				if ( this.enigma != 0 ) {

					switch ( this.enigma ) {
						case 1 : {
							// N % 3 == 1
							// 1 item in last page, display 2nd and 3rd items from previous page, remove 'third' class, add 'last'  class to last item
							this.listOffset = this.listOffset + (this.itemHeight * 2) + 3;
						} break;
						case 2 : {
							// N % 3 == 2
							// 2 items in last page, display 3rd items from previous page, remove 'third' class, add 'last'  class to last item
							this.listOffset = this.listOffset + this.itemHeight + 3;
						} break;
					}				
				}
			} else {
				this.enigma = 0;
			}

			// find items on selected page and load images
			this.pageImageLoad( pageIndex, this.enigma );
			this.list.css({"top" : this.listOffset + "px"});
		},

		/* --------------------------------------------------------------------------- */
		
		"itemReset" : function (itemIndex) {
			var that = this;

			this.list.find('.third,.last').animate({"top" : "0px"}, this.thirdSpeed, function(){
				$(this).css({"z-index" : ""});
			});
		},

		/* --------------------------------------------------------------------------- */
		
		"itemJump" : function (itemIndex) {
			var that = this;

			// handle opening / closing item info panel, plus map focus
			// convert item index to page index
			// direct to item -> newListOffset = poiIndex * -itemheight;
			// page -> newListOffset = (-itemheight * itemsperpage) * Math.floor(poiIndex / itemsperpage);
			this.currentPage = Math.floor(itemIndex / 3);
			this.pageJump( this.currentPage );
		},

		/* --------------------------------------------------------------------------- */
		
		"hideControls" : function () {
			var that = this;

			// this.controls.hide();
		},

		/* --------------------------------------------------------------------------- */
		
		"updateControls" : function () {
			var that = this;

			$('.results').removeClass('noarrows');

			if ( this.currentPage == 0) {
				this.upButton.hide();
			} else {
				this.upButton.show();
			}

			if ( this.currentPage == (this.pages - 1) ) {
				this.downButton.hide();
			} else {
				this.downButton.show();
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"addEntry" : function (entry) {
			var that = this;

			this.items.push(entry);
		},

		/* --------------------------------------------------------------------------- */
		
		"openEntry" : function (clickIndex) {
			var that = this,
				thisEntry = this.entries.eq(clickIndex),
				thisPlaceID = thisEntry.attr('data-place-id'),
				metaBlock = thisEntry.find('.meta'),
				metaSet = metaBlock.attr('data-meta');

			if ( TESTMODE ) { metaSet == 1; }

			if (metaSet == 1) {

				if ( this.currentPage == (this.pages - 1) && this.enigma != 0) {
					if ( thisEntry.hasClass('last') ) {
						// thisEntry.css({"top" : "-231px", "z-index" : "40"});
						thisEntry
						.css({"z-index" : "40"})
						.animate({"top" : "-245px"}, this.thirdSpeed, function(){});
					}
				} else {
					if ( thisEntry.hasClass('third') ) {
						// thisEntry.css({"top" : "-231px", "z-index" : "40"});
						thisEntry
						.css({"z-index" : "40"})
						.animate({"top" : "-245px"}, this.thirdSpeed, function(){});
					}
				}

				thisEntry.addClass('open');
				this.isOpen = true;
				this.openEntryIndex = clickIndex;

				// this.upButton.hide();
				// this.downButton.hide();
				$('.results').addClass('noarrows');

			} else {
				$('.results').addClass('noarrows');
				placesService.getDetails({"placeId" : thisPlaceID, "language" : this.language}, function (place, status) {
					if (status == 'OK') {
			            metaBlock.attr({ "data-meta" : "1",  "data-url" : place.url, "data-web" : place.website});

			            if ( place.hasOwnProperty('website') ) {
				            if ( place.website != '' ) {
								thisEntry.find('p.web a').attr({"href" : place.website});
				            } else {
				            	thisEntry.find('p.web').hide();
				            }
			            } else {
			            	thisEntry.find('p.web').hide();
			            }

						if ( that.currentPage == (that.pages - 1) && that.enigma != 0) {
							if ( thisEntry.hasClass('last') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						} else {
							if ( thisEntry.hasClass('third') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						}

						// phone number
						// place.formatted_phone_number
						// place.international_phone_number
						if ( place.hasOwnProperty('international_phone_number') ) {
							if ( place.international_phone_number != '') {
								thisEntry.find('p.phone span').text(place.international_phone_number);
							} else {
								thisEntry.find('p.phone').hide();
							}
						} else {
							thisEntry.find('p.phone').hide();
						}

						// opening times
						// places.opening_hours.weekday_text = []
						if ( place.opening_hours ) {
							if (place.opening_hours.weekday_text) {

								var times = place.opening_hours.weekday_text,
									out = thisEntry.find('div.ot ul'),
									newline,
									ti = 0,
									tc = times.length,
									tt;

								out.empty();
								do {
									tt = times[ti];

									newline = jQuery('<li></li>').text(tt);
									out.append(newline);

									ti++;
								} while (ti < tc)
								thisEntry.removeClass('noOpeningTimes');
							} else {
								thisEntry.addClass('noOpeningTimes');
								thisEntry.find('div.ot').hide();
							}
						} else {
							thisEntry.addClass('noOpeningTimes');
							thisEntry.find('div.ot').hide();
						}

						if ( that.currentPage == (that.pages - 1) && that.enigma != 0) {
							if ( thisEntry.hasClass('last') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						} else {
							if ( thisEntry.hasClass('third') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						}

						thisEntry.addClass('open');
						that.isOpen = true;
						that.openEntryIndex = clickIndex;

						$('.results').addClass('noarrows');

						// that.upButton.hide();
						// that.downButton.hide();

		        	}
				});
			}

		},

		/* --------------------------------------------------------------------------- */
		
		"closeEntry" : function () {
			var that = this,
				thisEntry = this.entries.eq(this.openEntryIndex);

			thisEntry
			.animate({"top" : "0px"}, this.thirdSpeed, function(){
				thisEntry.css({"z-index" : ""});
			});

			/*
			if ( this.currentPage == (this.pages - 1) && this.enigma != 0) {
				if ( thisEntry.hasClass('last') ) {
					// thisEntry.css({"top" : "-231px", "z-index" : "40"});
					thisEntry
					.animate({"top" : "0px"}, this.thirdSpeed, function(){
						thisEntry.css({"z-index" : ""});
					});
				}
			} else {
				if ( thisEntry.hasClass('third') ) {
					// thisEntry.css({"top" : "-231px", "z-index" : "40"});
					thisEntry
					.animate({"top" : "0px"}, this.thirdSpeed, function(){
						thisEntry.css({"z-index" : ""});
					});
				}
			}
			*/

			thisEntry.removeClass('open');
			this.isOpen = false;
			this.openEntryIndex = -1;
			this.updateControls();
			$('.results').removeClass('noarrows');
		},

		/* --------------------------------------------------------------------------- */
		
		"mapJump" : function (clickIndex) {
			var that = this,
			thisEntry = this.entries.eq(clickIndex),
	        poiLocation = new google.maps.LatLng( thisEntry.attr('data-lat'), thisEntry.attr('data-lng')),
	        poiPlaceId = thisEntry.attr('data-place-id');

	        this.markerChange(poiPlaceId);

	        googleMap.panTo( poiLocation ); 
	        if (zoomLevel != -1) {
	        	googleMap.setZoom(zoomLevel);	
	        }
	        
		},

		/* --------------------------------------------------------------------------- */
		
		"mapEvent" : function (clickIndex, place_id) {
			var that = this;

			this.itemJump(clickIndex);
			this.markerChange(place_id);

			// is there an entry open
			if ( !this.isOpen ) {
				//no
				this.openEntry(clickIndex);
			} else {
				// yes 
				// toggle state of selected entry
				if ( this.openEntryIndex != -1 ) {
					if ( this.openEntryIndex != clickIndex ) {
						// no, close open entry and open selected entry
						this.closeEntry();
						this.stopBounce();
						this.openEntry(clickIndex);
						this.markerChange(place_id);
					} else {
						this.closeEntry();
						this.stopBounce();
					}
				}
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"entryEvent" : function (entry) {
			var that = this,
				clickIndex = this.list.find('li.entry').not('li.template').index(entry);

			// console.log("HertzPlacesSidebar.entryEvent: %s", clickIndex);

			// is there an entry open
			if ( !this.isOpen ) {
				// no
				this.openEntry(clickIndex);
				var pause = window.setTimeout(function(){
					that.mapJump(clickIndex);	
				}, 300);
				
			} else {
				// yes
				// did the uswer click on the open entry?
				if ( this.openEntryIndex == clickIndex ) {
					// yes, close open entry
					this.closeEntry();
					this.stopBounce();
				} else {
					// no, close open entry and open selected entry
					this.closeEntry();
					this.stopBounce();
					this.openEntry(clickIndex);
					var pause = window.setTimeout(function(){
						that.mapJump(clickIndex);	
					}, 300);
				}
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"closeEvent" : function () {
			var that = this;

			this.closeEntry();
			this.stopBounce();
		},

		/* --------------------------------------------------------------------------- */
		
		"markerChange" : function (poiPlaceId) {
			var that = this;

	        if ( this.bounceMarkerIndex == -1 ) {
	        	// no map markers currently bouncing
	        	// start bounce animation
	        	this.startBounce(poiPlaceId);
	        } else {
	        	// stop current bounce
	        	this.stopBounce();
	        	// is the current bounce on the selected poi marker?
	        	if ( poiPlaceId != this.bounceMarkerId ) {
	        		// no, start a new bounce
	        		this.startBounce(poiPlaceId);
	        	}	
	        }
	    },

		/* --------------------------------------------------------------------------- */
		
		"stopBounce" : function () {
			var that = this;

        	var bouncingMarker = poiMarkers[this.bounceMarkerIndex];
        	// bouncingMarker.setAnimation(null);
			bouncingMarker.setZIndex(20);
			bouncingMarker.icon = mapIcons[bouncingMarker.icontype];
        	this.bounceMarkerIndex= -1;
        	this.bounceMarkerId = '';
		},

		/* --------------------------------------------------------------------------- */
		
		"startBounce" : function (poiPlaceId) {
			var that = this;

			var poiIndex = 0, poiCount = poiMarkers.length, tpoi1, tpoi2 = null;

			do {
				tpoi1 = poiMarkerIndex[poiIndex];

				if ( tpoi1.place_id == poiPlaceId) {
					tpoi2 = poiMarkers[tpoi1.index];
					break;
				}
				poiIndex++;
			} while ( poiIndex < poiCount )

			if (typeof tpoi2 == 'object' ) {

				tpoi2.setZIndex(100);
				// tpoi2.setAnimation(google.maps.Animation.BOUNCE);
				tpoi2.icon = mapIcons[tpoi2.icontype + '_large'];
				that.bounceMarkerIndex = tpoi1.index;
				that.bounceMarkerId = tpoi1.place_id;
			}
			/*
			poiMarkers.map(function(poiMarker,index){
				if ( poiPlaceId == poiMarker.place_id ) {
					console.log("marker found index: %s name: '%s' id: %s", index, poiMarker.place_name, poiMarker.place_id);
					poiMarker.setAnimation(google.maps.Animation.BOUNCE);
					that.bounceMarkerIndex = index;
					that.bounceMarkerId = poiPlaceId;
				}
			});
			*/

		},

		/* --------------------------------------------------------------------------- */
		
		"pageImageLoad" : function (page,enigma) {
			var that = this;

			var itemNodes = this.list.find('li.entry').not('li.template'),
				thisNode,
				itemoffset,
				items = [],
				photo,
				cb = new Date();

			itemoffset = (page * 3);
			items.push(itemoffset);
			items.push(itemoffset + 1);
			items.push(itemoffset + 2);

			// iterate through POI items in page
			items.map(function(item,index){
				thisNode = itemNodes.eq(item);

				that.imageLoaders.push(new PoiImageLoader({
					"item" : thisNode
				}));

			});
		},

		/* --------------------------------------------------------------------------- */
		
		"clear" : function () {
			var that = this;

			this.list.find('li.entry').not('li.template').remove();
			this.list.css({"top" : "0px"});

			this.items = [];
			this.entries;
			this.count = 0;

			this.pages = 0;
			this.currentPage = 0;
			this.listOffset = 0;

			this.isOpen = false;
			this.openEntryIndex = -1;

			that.imageLoaders = [];

			// this.upButton.hide();
			// this.downButton.hide();
			$('.results').addClass('noarrows');
		}

		/* --------------------------------------------------------------------------- */
	}

	window.HertzPlacesSidebar = HertzPlacesSidebar;

})();
(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzPlacesSidebarItem
	/* ------------------------------------------------ */
	
	function HertzPlacesSidebarItem(parameters) {
		this.parameters = parameters;
		this.sidebar = parameters.sidebar;
		this.list = parameters.list;
		this.item = parameters.item;

		this.group;
		this.isOpen = false;
		this.openIndex = -1;

		this.init();
	}
	
	HertzPlacesSidebarItem.prototype = {
		"constructor" : HertzPlacesSidebarItem,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			this.build();
		},

		/* --------------------------------------------------------------------------- */
		
		"build" : function () {
			var that = this,
				typeTitle,
				typeIcon,
				newPOINode,
				photoURL1,
				photoURL1x,
				photoURL1a,
				photoURL1b,
				photoURL1c,
				photoURL2;

			this.group = this.item.group;

			// set icon
	        typeTitle = sidebarIcons[this.group].title;
	        typeIcon = sidebarIcons[this.group].img;

		    sidebarIcons
		    newPOINode = this.list.find('li.template').clone().removeClass('template');

		    // set background-image
		    if (this.item.photos) {
		      if (this.item.photos.length > 0 ) {
		      	// plaes search results photo property -> photos[0].getUrl()
		      	photoURL1 = this.item.photos[0].getUrl({ "maxWidth" : 512 }); // curently 5/6/2017 returns invalid URL

		      	newPOINode
		      	.attr({"data-photo" : photoURL1});
		      }
		    }

		    // set main data and bind events
		    newPOINode.addClass(typeTitle);
		    newPOINode
		    .attr({
				"data-place-sd" : Math.floor(this.item.sd), // distance from placesService.nearbySearch centre in metres
				"data-place-id" : this.item.place_id,
				"data-rating" : this.item.rating,
				"data-lat" : this.item.geometry.location.lat(),
				"data-lng" : this.item.geometry.location.lng(),
				"data-distance" : (this.item.distance / 1000) // convert to kilometers
		    });

		    var distanceString,km,m;

		    if ( this.item.distance > 1000 ) {
		    	km = Math.floor(this.item.distance / 1000);
		    	m = Math.floor(this.item.distance - (km * 1000));
		    	// distanceString = km + '.' + m + 'km';
		    	distanceString = km + 'km'; // displaydistance from start of route rounded to nearest kilometre
		    } else {
		    	distanceString = Math.floor(this.item.distance) + 'm';
		    }

		    // set text
		    newPOINode.find('a.opener').on('click', function(e){
		      e.preventDefault();

		      that.sidebar.entryEvent(jQuery(this).parent('li'));

		    });
		    newPOINode.find('a.closer').on('click', function(e){
		      e.preventDefault();

		      that.sidebar.closeEvent();

		    });
		    newPOINode.find('.main h2').text(this.item.name);
		    newPOINode.find('.main img.icon').attr({"src" : typeIcon, "alt" : typeTitle});

		    if ( this.item.vicinity && this.item.vicinity != '' ) {
		    	newPOINode.find('.main address').eq(0).text(this.item.vicinity);	
		    } else {
		    	newPOINode.find('.main address').eq(0).hide();
		    }

		    newPOINode.find('.infoOuter p.distance span').text(distanceString);

		    // add to list container
		    this.list.append(newPOINode);

		}

		/* --------------------------------------------------------------------------- */
	}

	window.HertzPlacesSidebarItem = HertzPlacesSidebarItem;

})();
(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzStateController
	/* ------------------------------------------------ */

	function HertzStateController(parameters) {
		this.parameters = parameters;

    this.language = parameters.language;

    this.pages = parameters.pages;

    this.formPage = parameters.formPage;
    this.formStartField = parameters.formStartField;
    this.formEndField = parameters.formEndField;
    this.formGoButton = parameters.formGoButton;

    this.mapPage = parameters.mapPage;
    this.mapStartField = parameters.mapStartField;
    this.mapEndField = parameters.mapEndField;
    this.mapGoButton = parameters.mapGoButton;
    this.mapResetButton = parameters.mapResetButton;

    this.mapContainer = parameters.mapContainer;
    this.mapNode = parameters.mapNode;
    this.sidebarContainer = parameters.sidebarContainer;
    this.sidebar = parameters.sidebar;

    this.defaultTypes = parameters.defaultTypes;
    
    this.geolocationEnabled = parameters.geolocationEnabled;
    this.geoPosition = parameters.geoPosition;
    this.searchWindowStartPercent = parameters.searchWindowStartPercent;
    this.searchWindowEndPercent = parameters.searchWindowEndPercent;
    this.searchSegments = parameters.searchSegments;

    this.mapIcons = parameters.mapIcons;
    this.sidebarIcons = parameters.sidebarIcons;

    this.brandFilters = parameters.brandFilters;

    this.currentContext = 'form';
    this.requiredTypes;
    this.foundTypes = {};

    this.startPoint;
    this.startPointLabel = '';
    this.startPointSet = false;
    this.startMarker;

    this.endPoint;
    this.endPointLabel = '';
    this.endPointSet = false;
    this.endMarker;

    this.maxRadius = 0;
    this.maxRadiusPoint = -1;

    this.subset = [];
    this.skipRadii = [];

    this.mapOpen = false;
    this.readyToSearch = false;

		this.setup();
	}
	
	HertzStateController.prototype = {
		"constructor" : HertzStateController,

		"template" : function () { var that = this; },

		"setup" : function () {
			var that = this;

	      this.setupForm({});

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setupForm" : function (parameters) {
	      var that = this;

	      this.formStartAC = this.bindAutocompleter({
	        "textbox" : this.formStartField,
	        "mode" : "start",
	        "context" : "form"
	      });

	      this.formEndAC = this.bindAutocompleter({
	        "textbox" : this.formEndField,
	        "mode" : "end",
	        "context" : "form"
	      });

	      this.formGoButton.on('click', function(e){
	        e.preventDefault();
	        if ( !$(this).hasClass('inactive') ) {
	          that.handleGoButton({});
	        }
	        
	      });
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setupMap" : function (parameters) {
	      var that = this;

	      // console.log("start:'%s' end:'%s'", this.startPointLabel, this.endPointLabel);

	      $('#' + this.mapStartField).val(this.startPointLabel);
	      $('#' + this.mapEndField).val(this.endPointLabel);

	      $('#map .filterList li').each(function(i){
	      	if ( $(this).attr('data-value') == currentFilter ) {
	      		var newCaption = $(this).text();
	      		$(this).addClass('checked');
	      		$('#map .filterList a.open.button').html(newCaption);
	      	}
	      });

	      // $('#map .filterList').removeClass('one').addClass('zero');

	      this.requiredTypes = ( currentFilter != '' ) ? [currentFilter] : [];

	      this.mapGoButton.removeClass('inactive');

	      if ( !TESTMODE ) {
	        this.mapStartAC = this.bindAutocompleter({
	          "textbox" : this.mapStartField,
	          "mode" : "start",
	          "context" : "map"
	        });

	        this.mapEndAC = this.bindAutocompleter({
	          "textbox" : this.mapEndField,
	          "mode" : "end",
	          "context" : "map"
	        });
	      }

	      this.mapGoButton.on('click', function(e){
	        e.preventDefault();
	        if ( !$(this).hasClass('inactive') ) {
	          that.handleGoButton({});
	        }
	        
	      });

	      this.mapResetButton.on('click', function(e){
	        e.preventDefault();
	        if ( !$(this).hasClass('inactive') ) {
	          that.fullReset({});
	        }
	        
	      });

	      // get location from data attributes
	      /*
	      replace with browser/device geolocation, requires https host
	      */
	      var mapCentre = this.setPoint(jQuery('#mapbox').attr('data-centre'));
	      var mapCenterLocation = new google.maps.LatLng( mapCentre.lat, mapCentre.lng);
	      mapOptions.center = mapCenterLocation;

	      // insert map
	      googleMap = new google.maps.Map(document.getElementById( this.mapNode ), mapOptions);

	      // click on map to set start and end points
	      googleMap.addListener('click', function(me) {
	          /*
	          me = google.maps.MouseEvent
	          me.latLng = google.maps.LatLng
	            [ me.latLng.lat(), me.latLng.lng() ]
	          */

	          // first click set start, second sets end
	          // if start marker has been removed, another click will set

	          if ( !that.startMarkerSet ) {
	          	console.log("start lat: %s lng: %s", me.latLng.lat(),me.latLng.lng());

	            that.startMarker = new google.maps.Marker({
	                "position" : me.latLng,
	                "map" : googleMap,
	                "draggable" : false,
	                "icon" : that.mapIcons['start'],
	                "zIndex" : -20
	            });
	            that.startMarkerSet = true;
	            that.startPoint = me.latLng;
	          } else if ( !that.endMarkerSet ) {
	          	console.log("end lat: %s lng: %s", me.latLng.lat(),me.latLng.lng());

	            that.endMarker = new google.maps.Marker({
	                "position" : me.latLng,
	                "map" : googleMap,
	                "draggable" : false,
	                "icon" : that.mapIcons['end'],
	                "zIndex" : -20
	            });
	            that.endMarkerSet = true;
	            that.endPoint = me.latLng;
	          }

	          if (that.startMarkerSet && that.endMarkerSet) {
	            that.mapGoButton.removeClass('inactive');            
	          }

	          me.stop();
	        });

	      // resize map if browser viewport resizes
	      $(window).resize(function(){
	      google.maps.event.trigger(googleMap, "resize");

	        if( that.startMarkerSet && that.endMarkerSet ) {
	          var routeBounds = new google.maps.LatLngBounds();
	          routeBounds.extend(that.startPoint);
	          routeBounds.extend(that.endPoint);
	          googleMap.fitBounds(routeBounds, 0);
	        }
	      });

	      this.directionsService = new google.maps.DirectionsService;
	      this.directionsDisplay = new google.maps.DirectionsRenderer({ "suppressMarkers" : true });
	      // this.directionsDisplay.setMap(googleMap);

	      placesService = new google.maps.places.PlacesService(googleMap);

	      this.SideBarModule = new HertzPlacesSidebar({
	        "language" : this.language,
	        "list" : $('div.results ul#poilist'),
	        "controls" : $('div.results a.arrows'),
	        "itemsPerPage" : 3,
	        "itemHeight" : 242,
	        "pageHeight" : (242 + 3) * 3
	      });

	      this.POIList = new HertzPlaces({});

	      this.mapOpen = true;
	      this.currentContext = 'map';      
	      filterContext = 'map';
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "bindAutocompleter" : function (parameters) {
	      var that = this,
	          newACtemp;

	      newACtemp = new google.maps.places.Autocomplete( (document.getElementById(parameters.textbox)), { "types" : ['geocode'], "language" : this.language});
	      console.log(this.language);
	      newACtemp.addListener('place_changed', function() {

	        try{
	          that.placeChange({
	            "ac" : this,
	            "mode" : parameters.mode,
	            "context" : parameters.context
	          });
	        }

	        catch (e) {
	          console.log(e);
	        }

	      });

	      return newACtemp;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setPoint" : function (pointString) {
	      var that = this,
	      pt = pointString.split(",");

	      return {"lat" : pt[0], "lng" : pt[1] };
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setRoutePoint" : function (parameters) {
	      var that = this;

	      switch( parameters.place ) {
	        case "start" : {
	          this.startPoint = new google.maps.LatLng( parameters.lat, parameters.lng );
	          this.startPointLabel = parameters.label;
	          this.startMarkerSet = true;
	        } break;
	        case "end" : {
	          this.endPoint = new google.maps.LatLng( parameters.lat, parameters.lng );
	          this.endPointLabel = parameters.label;
	          this.endMarkerSet = true;
	        } break;
	      }

	      if (this.startMarkerSet && this.endMarkerSet) {
	        switch( this.currentContext ) {
	          case "form" : {
	            this.formGoButton.removeClass('inactive');
	          } break;
	          case "map" : {

	            this.mapGoButton.removeClass('inactive');
	          } break;
	        }
	      }

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "placeChange" : function (parameters) {
	      var that = this,
	          newPlace = parameters.ac.getPlace(),
	          newPlaceLatLng;

	          if(newPlace !== null && typeof newPlace === "object") {
	              newPlaceLatLng = newPlace.geometry.location;

	              switch( parameters.mode ) {
	                case "start" : {
	                  this.startPoint = newPlaceLatLng;
	                  this.startPointLabel = newPlace.formatted_address;
	                  this.startMarkerSet = true;
	                } break;
	                case "end" : {
	                  this.endPoint = newPlaceLatLng;
	                  this.endPointLabel = newPlace.formatted_address;
	                  this.endMarkerSet = true;
	                } break;
	              }

	              if (this.startMarkerSet && this.endMarkerSet) {
	                switch( parameters.context ) {
	                  case "form" : {
	                    this.formGoButton.removeClass('inactive');
	                  } break;
	                  case "map" : {

	                    this.mapGoButton.removeClass('inactive');
	                  } break;
	                }
	              }
	          }  
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "handleGoButton" : function (parameters) {
	      var that = this,
	          filtersSelected = false;

	      // form context - switch to map, draw start, end, route and do search
	      // map context - draw start, end, route and do search

	      if ( this.mapOpen ) {
	        this.resetMap();
	      }
	      
	      this.sidebarContainer.removeClass('noroute').removeClass('nopoi').removeClass('notype').removeClass('initial').addClass('searching');

	      this.requiredTypes = ( currentFilter != '' ) ? [currentFilter] : [];
	      console.log(this.requiredTypes);
			/*
			this.requiredTypes = this.getSelectedFilters({
				"ids" : ( this.currentContext == 'form' ) ? this.formFilters : this.mapFilters
			});
			*/

	      filtersSelected = ( this.requiredTypes.length > 0);

	      switch ( this.currentContext ) {
	        case "form" : {

	          if ( filtersSelected ) {
	            this.showMap({});
	          } else {
	            // no filters selected, display error
	            that.mapError({"error" : "notype"});
	          }
	        } break;
	        case "map" : {

	          if ( !filtersSelected ) {
	            // no filters selected, display error
	            that.mapError({"error" : "notype"});
	          }

	        } break;
	      }

	      if ( filtersSelected ) {
	        if ( !TESTMODE ) {
				this.mapGoButton.addClass('inactive');
				this.getRoute({});
	        }
	        
	      }
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showMap" : function (parameters) {
	      var that = this;

	      this.formPage.hide();
	      this.mapPage.show();
	      this.setupMap({});
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "fullReset" : function (parameters) {
	      var that = this;

	      this.resetMap({});

	      // clear star and end markers
	      this.startPoint = null;
	      this.startPointLabel = '';
	      this.startMarkerSet = false;
	      this.endPoint = null;
	      this.endPointLabel = '';
	      this.endMarkerSet = false;

	      if ( !TESTMODE ) {
	        this.mapStartAC.set('place', null);
	        this.mapEndAC.set('place', null);

	      }

	      $( '#' + this.mapStartField ).val('');
	      $( '#' + this.mapEndField ).val('');

	      // reset category selection to all tyoes
	      $('#map .filterList input[type=checkbox]').each(function(i){
	          $(this).prop('checked', true);
	      });

	      this.sidebarContainer.removeClass('noroute').removeClass('nopoi').removeClass('notype').removeClass('searching').addClass('initial');
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "resetMap" : function (parameters) {
	      var that = this;

	      if ( this.startMarker ) {
	        this.startMarker.setMap(null);
	      }
	      
	      if ( this.endMarker ) {
	        this.endMarker.setMap(null);
	      }

	      if( this.directionsDisplay) {
	        this.directionsDisplay.setMap(null);
	      }

	      if ( poiMarkers.length > 0) {
	        var pmi = 0,
	            pmc = poiMarkers.length,
	            tpm;

	        do {
	          tpm = poiMarkers[pmi];

	          tpm.setMap(null);

	          pmi++;
	        } while (pmi < pmc)
	      }

	      // radii
	      if ( radii.length > 0 ) {
	        var ri = 0,
	            rc = radii.length,
	            tr;

	        do {
	          tr = radii[ri];

	          tr.setMap(null);

	          ri++;
	        } while (ri < rc)
	      }

	      this.originalPath = [];
	      this.workingPath = [];
	      this.subset = [];
	      this.foundTypes = {};

	      if ( this.POIList ) {
	        this.POIList.empty();
	      }
	      
	      if ( this.SideBarModule ) {
	        this.SideBarModule.clear();  
	      }
	      
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "getRoute" : function (parameters) {
	      var that = this;

	      this.directionsService.route(
	        {
	          "origin" : this.startPoint,
	          "destination" : this.endPoint,
	          "travelMode" : 'DRIVING',
	          "language" : this.language
	        },
	        function(response, status) {
	          if (status === 'OK') {

	            that.routeLength = response.routes[0].legs[0].distance.value;
	            that.routeSteps = response.routes[0].legs[0].steps.length;

	            console.log("this.routeLength: %skm this.routeSteps: %s", (that.routeLength / 1000), that.routeSteps);

	            that.readyToSearch = false;

	            that.drawRouteMarkers({});
	            that.drawRoute({"response" : response});
	            that.getSearchPoints({"response" : response});

	            /* */
	            if ( that.readyToSearch ) {
	            	that.buildPOISearchQueue({});	
	            }
	            /* */

	            /*
	            var routeBoxer = new RouteBoxer();
	            var path = response.routes[0].overview_path;
          		var boxes = routeBoxer.box(path, 5000);
          		console.log(boxes);
	            */

	          } else {
	            // inability to find route
	            that.mapError({"error" : "noroute"});
	          }
	        }
	      );

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "mapError" : function (parameters) {
	      var that = this;

	      switch ( parameters.error ) {
	        case "noroute" : {
	          console.log('ERROR: no route found');
	          this.sidebarContainer.removeClass('searching').addClass('noroute');
	        } break;
	        case "nopoi" : {
	          console.log('ERROR: no points of interest found');
	          this.sidebarContainer.removeClass('searching').addClass('nopoi');
	          this.mapGoButton.removeClass('inactive');
	        } break;
	        case "notype" : {
	          console.log('ERROR: no POI categories selected');

	          switch ( this.currentContext ) {
	            case "form" : {
	              // display filter error popup/message in form
	              $('.filterList').addClass('error');
	            } break;
	            case "map" : {
	              // display filter error in sidebar
	              this.sidebarContainer.removeClass('searching').addClass('notype');
	            } break;
	          }
	        } break;
	      }
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "drawRouteMarkers" : function (parameters) {
	      var that = this,
	          routeBounds;

	      this.startMarker = new google.maps.Marker({
			// "animation" : google.maps.Animation.BOUNCE,
			"position" : this.startPoint,
			"map" : googleMap,
			"draggable" : false,
			"icon" : this.mapIcons['start'],
			"zIndex" : -20,
			"optimized" : false
	      });

	      this.endMarker = new google.maps.Marker({
			// "animation" : google.maps.Animation.BOUNCE,
			"position" : this.endPoint,
			"map" : googleMap,
			"draggable" : false,
			"icon" : this.mapIcons['end'],
			"zIndex" : -20,
			"optimized" : false
	      });

	      // centre map on route
	      routeBounds = new google.maps.LatLngBounds();
	      routeBounds.extend(this.startPoint);
	      routeBounds.extend(this.endPoint);
	      googleMap.fitBounds(routeBounds, 0);
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "drawRoute" : function (parameters) {
	      var that = this;

	      this.directionsDisplay.setMap(googleMap);
	      this.directionsDisplay.setDirections(parameters.response);
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "getSearchPoints" : function (parameters) {
	      var that = this;

	      // parameters.response
	      // parameters.response.routes[0].overview_path

	      /*
	      this.originalPath, this.workingPath
	      [ { "lat" : LAT, "lng" : LNG }, { "lat" : LAT, "lng" : LNG }, ... ]
	      */

	      this.originalPath = this.storePathPoints(parameters.response.routes[0].overview_path);
	      this.workingPath = this.cloneArray(this.originalPath);

	      // this.savePOIList(this.originalPath, 'originalpath.json');

	      // this.trimPath();
	      // this.trimAreas();

	      this.segmentPath({
	      	"path" : parameters.response.routes[0].overview_path,
	      	"segments" : this.searchSegments
	      })
	      // this.showSearchAreas();
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "segmentPath" : function (parameters) {
			var that = this;
			/*
			in:
			parameters = {
				"path" : [{lat,lng}], // array of points = response.routes[0].overview_path
				"segments" : 12
			}
			out:
			this.subset
			*/

			var previousPoint = this.startPoint,
				ptpDistance = 0,
				totalDistance = 0;

			// calculate total route distance and set distance from start on each point in path
			parameters.path.map(function(point,index){
				ptpDistance = google.maps.geometry.spherical.computeDistanceBetween(previousPoint, point);
				totalDistance = totalDistance + ptpDistance;
				point.ptp = ptpDistance;
				point.pd = totalDistance;
				previousPoint = point;
			});

			// find beginning and end of search window, from 25% to 75% of total route
			var windowStartIndex = -1,
				windowEndIndex = -1,
				windowStartDistance = totalDistance * (this.searchWindowStartPercent / 100),
				windowEndDistance = totalDistance * (this.searchWindowEndPercent / 100),
				windowDistance = windowEndDistance - windowStartDistance;

			parameters.path.map(function(point,index){
				if (windowStartIndex == -1 && point.pd > windowStartDistance) {
					windowStartIndex = index;
				}
				if (windowEndIndex == -1 && point.pd > windowEndDistance) {
					windowEndIndex = index;
				}
			});

			// plot start and end of window
			var windowStart = parameters.path[windowStartIndex],
				windowEnd = parameters.path[windowEndIndex];

			// trim path down to window
			var spliceCount = windowEndIndex - windowStartIndex;
			var windowPath = parameters.path.splice(windowStartIndex, spliceCount);

			var segmentLength = windowDistance / parameters.segments,
				segmentNodes = [],
				sd = windowStart.pd,
				wd = 0;

			segmentNodes.push(windowStart);
			windowPath.map(function(point,index){

				wd = point.pd - sd;
				if ( wd > segmentLength ) {
					segmentNodes.push(point);
					sd = sd + wd;
				}
			});

			/* */
	        // console.log("length of window: %s", windowDistance);
			/* */

			segmentNodes.map(function(point){
				point.pr = segmentLength;
			});
	        this.subset = segmentNodes;
	        this.readyToSearch = true;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "rhumbDestinationPoint" : function (parameters) {
			var that = this;

			/*
			parameters = {
				"point" : LatLng
				"bearing" : 0,
				"distance" : 500
			}
			*/

			var R = 6371, // earth's mean radius in km
				d = parseFloat(parameters.distance) / R,  // d = angular distance covered on earth's surface
				lat1,
				lon1,
				bearing;

			bearing = parameters.bearing.toRad();
			lat1 = parameters.point.lat().toRad();
			lon1 = parameters.point.lng().toRad();

			var lat2 = lat1 + d * Math.cos(bearing);
			var dLat = lat2 - lat1;
			var dPhi = Math.log(Math.tan(lat2 / 2 + Math.PI / 4) / Math.tan(lat1 / 2 + Math.PI / 4));
			var q = (Math.abs(dLat) > 1e-10) ? dLat / dPhi : Math.cos(lat1);
			var dLon = d * Math.sin(bearing) / q;
			// check for going past the pole
			if (Math.abs(lat2) > Math.PI / 2) {
				lat2 = lat2 > 0 ? Math.PI - lat2 : - (Math.PI - lat2);
			}
			var lon2 = (lon1 + dLon + Math.PI) % (2 * Math.PI) - Math.PI;

			if (isNaN(lat2) || isNaN(lon2)) {
				return null;
			}
			return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "trimPath" : function (parameters) {var that = this;},

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "trimAreas" : function (parameters) {var that = this;},

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildPOISearchQueue" : function (parameters) {
	      var that = this,
	          superThat = this;

	      var filters = (this.requiredTypes.length == 1) ? this.requiredTypes : [];
	      var workingSubset = this.cloneArray(this.subset);
	      
	      var queueParameters = {
	        "queue" : workingSubset, // create clone of subset array
	        "worker" : {
	          "global" :{
	            "language" : this.language,
	            "radius" : 500, // 500 metres
	            "type" : filters
	          },
	          "action" : function(){
	            var that = this,
	                pointLatLng;
	            var waiting = window.setTimeout(function(){

					pointLatLng = new google.maps.LatLng( that.settings.item.lat(), that.settings.item.lng() );

					superThat.showSearchArea({
						"point" : pointLatLng,
						"radius" : that.settings.item.pr
					});

					placesService.nearbySearch({
						"location" : pointLatLng,
						"radius" : that.settings.item.pr,
						// "bounds" : that.settings.item,
						"type" : that.settings.global.type,
						"language" : that.settings.global.language
					},
					function( results, status ){

						if ( status == 'OK' ) {
							var ri = 0,
							rc = results.length,
							tr,
							trTypeStatus;

							do {
								tr = results[ri];

								// unrated places go to end of list
								if (!tr.hasOwnProperty('rating')) {
									tr.rating = 0;
								}

								tr.xd = pointLatLng;
								tr.sd = google.maps.geometry.spherical.computeDistanceBetween(pointLatLng,tr.geometry.location);
								// capture POI if it matches one of the required types
								// trTypeStatus = testTypes(tr.types, requiredTypes);

								// store POI if it matches one of default types
								trTypeStatus = superThat.testFilters({
									"itemTypes" : tr.types,
									"typepool" : superThat.defaultTypes
								});
								tr.group = trTypeStatus.type;
								tr.distance = that.settings.item.pd; // store distance from start of route in POI data
								if ( trTypeStatus.isMatch ) {
									superThat.POIList.addPlace(tr);  
								}

								ri++;
							} while ( ri < rc )
						}              
						that.completed();

					});
	            }, 500); // limit requests per second to prevent exceeding request per second quota

	          },
	          "callback" : function(){
	            this.queue.workerCompleted();
	          }
	        },
	        "finished" :  function(){
	            var poiCount = superThat.POIList.getCount();
	            console.log( "POI found: %s", poiCount );

	            if ( poiCount > 0 ) {
	              superThat.showPOIs();
	            } else {
	              superThat.mapError({"error" : "nopoi"});
	            }          
	        }
	      };
	      this.queue = new Queue(queueParameters);
	      this.queue.startWorker();

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showPOIs" : function (parameters) {
			var that = this;

			this.sidebarContainer.removeClass('searching').addClass('blank');

			var pi = 0,
				pc,
				allPOI,
				tp,
				workingPOI = [];

			this.foundTypes = this.POIList.typeFilter(this.requiredTypes);
			this.POIList.brandFilter({
				"requiredTypes" : this.requiredTypes,
				"filters" : this.brandFilters
			});
			this.POIList.ratingSort();

			workingPOI = this.POIList.getPlaces();

			if ( workingPOI.length > 0 ) {
				this.buildPOIMarkers({"items" : workingPOI});
				this.buildSidebar({"items" : workingPOI});

				var wait = window.setTimeout(function() {

					// three cases
					// N % 3 == 0
					// last item is third item

					// mark every third entry - open info panel in different manner for last itme in page
					that.sidebar.find('li.entry').not('li.template').each(function(i){
					if ( (i + 1) % 3 == 0 ) {
					  $(this).addClass('third');
					}
					});

					that.sidebar.find('li.entry').not('li.template').eq(that.sidebar.find('li.entry').not('li.template').length - 1).addClass('last');

					// update sidebor controls
					that.SideBarModule.configure();
					that.SideBarModule.pageImageLoad(0,0);
					that.mapGoButton.removeClass('inactive');

				},100);
			} else {
				console.log('x');
				this.mapError({"error" : "nopoi"});
			}

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildPOIMarkers" : function (parameters) {
	      var that = this;

	      var pi = 0, pc = parameters.items.length,tp;

	      do {
	        tp = parameters.items[pi];

	        this.buildPOIMarker({
	          "latlng" : tp.geometry.location,
	          "icon" : this.mapIcons[tp.group],
	          "name" : tp.name,
	          "type" : tp.group,
	          "place_id" : tp.place_id 
	        });

	        pi++;
	      } while (pi < pc)
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildPOIMarker" : function (parameters) {
	      var that = this;

	      var newMarker = new google.maps.Marker({
	        "icon" : parameters.icon,
	        "position" : parameters.latlng,
	        "map" : googleMap,
	        "draggable" : false,
	        "zIndex" : -20,
	        "place_name" : parameters.name,
	        "place_id" : parameters.place_id,
	        "icontype" : parameters.type
	      })

	      newMarker.addListener('click', function(poiMarker) {
	        var clickName = this.place_name,
	            clickid = this.place_id,
	            clickLatLng = this.position,
	            clickedpoi,
	            clickedpoiIndex;

	        // console.log("POI marker: '%s' place_id: %s", clickName, clickid);
	        // identify where in the results list this POI is and scroll to it
	        clickedpoi = $('#poilist li[data-place-id="' + clickid + '"]');
	        // clickedpoi.find('.frame').addClass('shake');
	        clickedpoiIndex = $('#poilist li.entry').not('li.template').index(clickedpoi);

	        // scroll to page in sidebar
	        that.SideBarModule.mapEvent(clickedpoiIndex,clickid);

	        googleMap.panTo( clickLatLng );
	        if (zoomLevel != -1) {
	        	googleMap.setZoom(zoomLevel);	
	        }

	      });

	      poiMarkers.push(newMarker);
	      poiMarkerIndex.push({
	      	"index" : poiMarkers.length -1,
	      	"place_id" : parameters.place_id
	      });

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildSidebar" : function (parameters) {
	      var that = this;

	      var pi = 0, pc = parameters.items.length,tp,tpLatLng;

	      do {
	        tp = parameters.items[pi];

	        this.SideBarModule.addEntry( new HertzPlacesSidebarItem({
	          "sidebar" : this.SideBarModule,
	          "list" : this.sidebar,
	          "item" : tp
	        }) );
	        
	        pi++;
	      } while (pi < pc)

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showSearchAreas" : function (parameters) {
	      var that = this,
	          si = 0,
	          sc = this.subset.length,
	          ts,
	          tsLatLng;

	      do {
	        ts = this.subset[si];
	        tsLatLng = new google.maps.LatLng( ts.lat, ts.lng );

	        this.showSearchArea({
	          "point" : tsLatLng,
	          "radius" : ts.pr
	        });

	        si++;
	      } while ( si < sc )
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showSearchArea" : function (parameters) {
	      var that = this;

	      radii.push(new google.maps.Circle({
	        "strokeColor" : HertzYellow,
	        "strokeOpacity" : 0.8,
	        "strokeWeight" : 2,
	        "fillColor" : HertzYellow,
	        "fillOpacity" : 0.35,
	        "map" : googleMap,
	        "center" : parameters.point,
	        "radius" : parameters.radius // radius in metres
	      }));
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "getSelectedFilters" : function (parameters) {
	      var that = this,
	          filterIndex = 0,
	          thisFilter,
	          foundTypes = [];

	      do {
	        thisFilter = $(parameters.ids[filterIndex]);

	        if ( thisFilter.is(':checked') ) {
	          foundTypes.push(thisFilter.val());
	        }
	        filterIndex++;
	      } while ( filterIndex < parameters.ids.length )

	      return foundTypes;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "testFilters" : function (parameters) {
	      /*
	      parameters.itemTypes
	      parameters.typepool
	      */
	      var that = this,
	          rtIndex = 0,
	          rtCount = parameters.typepool.length,
	          rtItem,
	          result = {
	            "isMatch" : false,
	            "type" : "unknown"
	          };

	      do {
	        rtItem = parameters.typepool[rtIndex];

	        if ( parameters.itemTypes.indexOf(rtItem) != -1 ) {
	          result = {
	            "isMatch" : true,
	            "type" : rtItem 
	          };
	          break;
	        }
	        rtIndex++;
	      } while ( rtIndex < rtCount) 

	      return result;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "storePathPoints" : function (path) {
	      var that = this,
	          pi = 0,
	          pc = path.length,
	          tp,
	          newPath = [];

	      do {
	        tp = path[pi];

	        newPath.push({
	          "lat" : tp.lat(),
	          "lng" : tp.lng()
	        });

	        pi++;
	      } while (pi < pc)

	      return newPath;
		}, 

		/* --------------------------------------------------------------------------- */

		"quickSort" : function  (arr, left, right) {
			var i = left,
				j = right,
				tmp,
				pivotidx = (left + right) / 2,
				pivot = arr[pivotidx.toFixed()];  

			/* partition */
			while (i <= j)
			{
				// ascending order
				while (arr[i] < pivot) {
					i++;
				}

				while (arr[j] > pivot) {
					j--;
				}

				// descending order
				/*
				while (arr[i] > pivot) {
					i++;
				}

				while (arr[j] < pivot) {
					j--;
				}
				*/

				if (i <= j)
				{
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;

					i++;
					j--;
				}
			}

			/* recursion */
			if (left < j)
				this.quickSort(arr, left, j);
			if (i < right)
				this.quickSort(arr, i, right);
			return arr;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "cloneArray" : function (array) {
	      var that = this,
	          ai = 0,
	          ac = array.length,
	          ta,
	          newArray = [],
	          newObject;

	      do {
	        ta = array[ai];

	        newObject = {};

	        Object.keys(ta).map(function(property){
	        	newObject[property] = ta[property];
	        });
	        newArray.push(newObject);

			/* */

	        ai++;
	      } while (ai < ac)

	      return newArray;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "savePOIList" : function (variable,filename) {
	      var that = this;

	      var hiddenElement = document.createElement('a');
	      hiddenElement.href = 'data:attachment/text,' + encodeURI( JSON.stringify( variable ) );
	      hiddenElement.target = '_blank';
	      hiddenElement.download = filename;
	      hiddenElement.click();
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "testSearch" : function (parameters) {
			var that = this;

			// parameters.latlng
			placesService.nearbySearch({
				"location" : parameters.latlng,
				"radius" : 5000,
				// "bounds" : that.settings.item,
				"type" : ['natural_feature'],
				"language" : "en"
			}, function( results, status ){

				googleMap.panTo(parameters.latlng);

				var newMarker = new google.maps.Marker({
					"position" : parameters.latlng,
					"map" : googleMap,
					"draggable" : false,
					"zIndex" : -20
				});

				radii.push(new google.maps.Circle({
					"strokeColor" : HertzYellow,
					"strokeOpacity" : 0.8,
					"strokeWeight" : 2,
					"fillColor" : HertzYellow,
					"fillOpacity" : 0.35,
					"map" : googleMap,
					"center" : parameters.latlng,
					"radius" : 5000 // radius in metres
				}));

				// that.savePOIList(results, 'margate.json');
			});


		}

		/* ---------------------------------------------------------------------------------------------------------------- */
	}

	window.HertzStateController = HertzStateController;

})();
(function () {
	"use strict";

	/* ------------------------------------------------ */
	// PoiImageLoader
	/* ------------------------------------------------ */
	
	function PoiImageLoader(parameters) {
		this.parameters = parameters;
		this.item = parameters.item;

		this.init();
	}
	
	PoiImageLoader.prototype = {
		"constructor" : PoiImageLoader,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			this.photo = this.item.attr("data-photo");

			// does the point of interest have an image?
			if ( typeof this.photo != 'undefined' ) {

				// yes
		        this.imageTimer = window.setTimeout(function(){
		        	that.imageTimeout();
		        }, 750);

		        try{
					// jquery 2.x
					/*
					var imgLoader = $('<img></img>')
			        .load( function(e){
						console.log('PoiImageLoader 39');
			        	that.imageLoad();
			        })
			        .attr({"src" : this.photo});
					*/

					// jquery 3.x
					var imgLoader = $('<img></img>')
			        .on('load', function(e){
			        	that.imageLoad();
			        })
			        .attr({"src" : this.photo});

					this.item
			        .find('.frameInner').append(imgLoader);
		        }
		        catch(e){
		        }

			} else {
				// no, fade in item
				this.item.removeClass('waitingForImage');
			}
		},

		/* --------------------------------------------------------------------------- */

		"imageLoad" : function () {
			var that = this;

        	this.item
	        .find('.frameInner')
	        .css({"background-image" : "url(" + this.photo + "?time=" + Date.now() + ")"});  

			this.item.removeClass('waitingForImage').addClass("imageLoaded");
			clearTimeout(this.imageTimer);
		},

		/* --------------------------------------------------------------------------- */

		"imageTimeout" : function () {
			var that = this;

			this.item.removeClass('waitingForImage');
		}

		/* --------------------------------------------------------------------------- */
	}

	window.PoiImageLoader = PoiImageLoader;

})();
(function () {
	"use strict";

	/* ------------------------------------------------ */
	// Queue
	/* ------------------------------------------------ */
	
	function Queue(parameters) {
		this.parameters = parameters;
		this.queue = parameters.queue;
		this.worker = parameters.worker;
		this.finished = parameters.finished;
	
		this.queueLimit = this.queue.length;
		this.workerBox = [];
		this.workerRunning = false;

		this.init();
	}
	
	Queue.prototype = {
		"constructor" : Queue,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

		},
		"startWorker" : function () {
			var that = this,
				newItem,
				newWorker,
				waiting = [],
				waiting2 = [];


			if ( this.queueLimit > 0 ) {
				// newItem = this.queue.pop(); // get LAST element in queue
				newItem = this.queue.splice(0,1); // get FIRST item in queue

				this.queueLimit = this.queue.length;

				this.workerRunning = true;

				newWorker = new Worker({
					"queue" : this,
					"settings" : {
						"global" : that.worker.global,
						"item" : newItem[0]
					},
					"action" : that.worker.action,
					"callback" : that.worker.callback
				});
			}
		},
		"workerCompleted" : function () {
			var that = this;

			if ( this.queueLimit > 0 ) {
				this.startWorker();
			}

			if ( !this.workerRunning) {
				this.queueCompleted();
			}

			if ( this.queueLimit == 0 && this.workerRunning) {
				this.workerRunning = false;
			}


		},
		"queueCompleted" : function () {
			var that = this;

			this.finished();
		}
	};

	window.Queue = Queue;
})();

(function () {
	"use strict";

	/* ------------------------------------------------ */
	// Worker
	/* ------------------------------------------------ */

	function Worker(parameters) {
		this.parameters = parameters;

		this.settings = parameters.settings;
		this.queue = parameters.queue; // link back to parent queue object
		this.action = parameters.action;
		this.callback = parameters.callback;
	
		this.start();
	}
	
	Worker.prototype = {
		"constructor" : Worker,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;

			this.action(this);
		},
		"completed" : function () {
			var that = this;

			this.callback(this, this.queue);
		}
	};
	
	window.Worker = Worker;

})();
// September 2019 localhost
// console.cloud.google.com/apis/dashboard
var  APIKEY = "AIzaSyDkMRgRp5PfXcajLCwM_uVE64s8L-nkUEg"; 


	// vivaldi
	// specify width and height attributes in svg 
	// <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 125" style="enable-background:new 0 0 100 125;" width="100px" height="125px" xml:space="preserve">

	// firefox 
	// specify width and height in svg,
	// specify color declaration using rgb(R,G,B) not #RRGGBB as firefox chokes on '#' characters in svg files
	// specify "size" property in icon declaration

	// set up default icon
	var mapIcons = [],
		anchor = {
			"normal" : [22,50],
			"large" : [32,75]
		},
		size = [100, 125],
		scaledSize = {
			"normal" : [43,59],
			"large" : [65,88]
		};

	function buildIcons(type) {
		var iconDefs = [];

		iconList.map(function(icon){
			var tempIcon = {},
				activeType = icon[type];

			tempIcon.url = activeType.url;

			if (activeType.size.length > 0 ) {
				tempIcon.size = new google.maps.Size(activeType.size[0],activeType.size[1]);
			}

			if (activeType.scaled.length > 0 ) {
				tempIcon.scaledSize = new google.maps.Size(activeType.scaled[0],activeType.scaled[1]);
			}

			if (activeType.anchor.length > 0 ) {
				tempIcon.anchor = new google.maps.Point(activeType.anchor[0],activeType.anchor[1]);
			}
			
			iconDefs[icon.key] = tempIcon;
		});

		return iconDefs;
	}

	var iconList = [
		{
			"key" : "default",
			"svg" : {
				"url" : "assets/svg/white-google-map-pin.svg",
				"size" : [],
				"anchor" : [17,50],
				"scaled" : [60,60]
			},
			"png" : {
				"url" : "",
				"size" : [],
				"anchor" : [],
				"scaled" : [60,60]
			}
		},
		{
			"key" : "start",
			"svg" : {
				"url" : "assets/svg/green-google-map-pin.svg",
				"size" : [],
				"anchor" : [17,50],
				"scaled" : [60,60]
			},
			"png" : {
				"url" : "assets/img/icons/map/start.png",
				"size" : [56,70],
				"anchor" : [16,40],
				"scaled" : [32,40]
			}
		},
		{
			"key" : "end",
			"svg" : {
				"url" : "assets/svg/red-google-map-pin.svg",
				"size" : [],
				"anchor" : [17,50],
				"scaled" : [60,60]
			},
			"png" : {
				"url" : "assets/img/icons/map/end.png",
				"size" : [56,70],
				"anchor" : [16,40],
				"scaled" : [32,40]
			}
		},
		{
			"key" : "art_gallery",
			"svg" : {
				"url" : "assets/svg/map/art_gallery.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/gallery.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "art_gallery_large",
			"svg" : {
				"url" : "assets/svg/map/art_gallery.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/gallery.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "museum",
			"svg" : {
				"url" : "assets/svg/map/museum.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/museum.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "museum_large",
			"svg" : {
				"url" : "assets/svg/map/museum.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/museum.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "restaurant",
			"svg" : {
				"url" : "assets/svg/map/restaurant.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/restaurant.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "restaurant_large",
			"svg" : {
				"url" : "assets/svg/map/restaurant.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/restaurant.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "spa",
			"svg" : {
				"url" : "assets/svg/map/spa.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/spa.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "spa_large",
			"svg" : {
				"url" : "assets/svg/map/spa.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/spa.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "stadium",
			"svg" : {
				"url" : "assets/svg/map/stadium.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/stadium.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "stadium_large",
			"svg" : {
				"url" : "assets/svg/map/stadium.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/stadium.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "park",
			"svg" : {
				"url" : "assets/svg/map/park.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/park.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "park_large",
			"svg" : {
				"url" : "assets/svg/map/park.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/park.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "zoo",
			"svg" : {
				"url" : "assets/svg/map/zoo.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/zoo.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "zoo_large",
			"svg" : {
				"url" : "assets/svg/map/zoo.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/zoo.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "amusement_park",
			"svg" : {
				"url" : "assets/svg/map/amusement_park.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/amusement_park.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "amusement_park_large",
			"svg" : {
				"url" : "assets/svg/map/amusement_park.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/amusement_park.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "bakery",
			"svg" : {
				"url" : "assets/svg/map/bakery.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/bakery.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "bakery_large",
			"svg" : {
				"url" : "assets/svg/map/bakery.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/bakery.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "cafe",
			"svg" : {
				"url" : "assets/svg/map/cafe.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/cafe.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "cafe_large",
			"svg" : {
				"url" : "assets/svg/map/cafe.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/cafe.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		}
	];

	var sidebarIcons = [];

	sidebarIcons['art_gallery'] = {
		"title" : "gallery",
		"img": "assets/img/icons/sidebar/gallery.png"
	};

	sidebarIcons['museum'] = {
		"title" : "museum",
		"img": "assets/img/icons/sidebar/museum.png"
	};

	sidebarIcons['restaurant'] = {
		"title" : "restaurant",
		"img": "assets/img/icons/sidebar/restaurant.png"
	};
	sidebarIcons['spa'] = {
		"title" : "spa",
		"img": "assets/img/icons/sidebar/spa.png"
	};
	sidebarIcons['stadium'] = {
		"title" : "stadium",
		"img": "assets/img/icons/sidebar/stadium.png"
	};
	sidebarIcons['amusement_park'] = {
		"title" : "amusement park",
		"img": "assets/img/icons/sidebar/amusement_park.png"
	};
	sidebarIcons['park'] = {
		"title" : "park",
		"img": "assets/img/icons/sidebar/park.png"
	};
	sidebarIcons['zoo'] = {
		"title" : "zoo",
		"img": "assets/img/icons/sidebar/zoo.png"
	};
	sidebarIcons['bakery'] = {
		"title" : "bakery",
		"img": "assets/img/icons/sidebar/bakery.png"
	};
	sidebarIcons['cafe'] = {
		"title" : "cafe",
		"img": "assets/img/icons/sidebar/cafe.png"
	};

var supportsCSSTransitions, supportsCSSAnimation, supportsGelocation, supportsSVG, supportsSVGforeignObject, isIE, pngTest, ishighdpi, expanderOpen = false, mainStateController;
var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

var googleMap,
	mapOptions = {
		// center: mapCenterLocation,
		// mapTypeId: "BW",
		// mapTypeControlOptions: {mapTypeIds: ["BW"]},
		"mapTypeId" : "hybrid",
		"zoom" : 12,
	},
	filterContext = 'form',
	currentFilter = '',
	HertzYellow = "#ffcf2c",
	radii = [],
	markers = [],
	poiMarkers = [],
	poiMarkerIndex = [],
	lines = [],
	boxPolys = [],
	placesService,
	windowSize,
	zoomLevel = -1, // HertzPlacesSidebar.mapJump, HertzStateController.buildPOIMarker, -1 turns off zoom change
	TESTMODE = false;

	/* Forli to Milan thanks katerina sforza */
	var forli = {
		"label" : "Forli, Province of Forli-Cesena, Italy",
		"lat" : 44.2227398,
		"lng" : 12.040731199999982
	};
	var ovierto = {
		"label" : "05018 Orvieto, Province of Terni, Italy",
		"lat" : 42.7185068,
		"lng" : 12.110744599999975
	};

(function () {
	"use strict";

	Number.prototype.toRad = function () {
	  return this * Math.PI / 180;
	};

	Number.prototype.toDeg = function () {
	  return this * 180 / Math.PI;
	};

	Number.prototype.toBrng = function () {
	  return (this.toDeg() + 360) % 360;
	};

	window.requestAnimFrame = (function(callback) {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
	})();

	if (!window.console) {
		window.console = {
			"log" : function(){}
		}
	}

	/* ---------------------------------------------------------------------------------- */
	// document ready START
	/* ---------------------------------------------------------------------------------- */

	/* */
	/*
	window.onload = function(e){};
	*/
	/* */	

	$(document).ready(function () {

		supportsCSSAnimation = $('html').hasClass('cssanimations');
		supportsCSSTransitions = $('html').hasClass('csstransitions');
		supportsGelocation = $('html').hasClass('geolocation');
		supportsSVG = $('html').hasClass('svg');
		supportsSVGforeignObject = $('html').hasClass('svgforeignobject');
		isIE = $('html').hasClass('ie') || isIE11;

		/*
		windowSize = {
			"w" : $(window).width(),
			"h" : $(window).height()
		};

		$(window).resize(function(){
			var throttle = window.setTimeout(function(){
				windowSize = {
					"w" : $(window).width(),
					"h" : $(window).height()
				};
				//console.log("w: %s h: %s", windowSize.w, windowSize.h );
			}, 500);
		});
		*/
		ishighdpi = ( window.devicePixelRatio > 1);

		// browser geolocation API support
		// http://caniuse.com/#feat=geolocation

		if ( supportsGelocation ) {
			/* geolocation is available */
			// console.log('supports geolocation');

			// https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation
			// https://stackoverflow.com/questions/17402103/google-api-for-location-based-on-user-ip-address

			// requires https host
			/*  */
			navigator.geolocation.getCurrentPosition(function(position) {
				console.log(position.coords.latitude, position.coords.longitude);
			});
			/*  */

		} else {
		  /* geolocation IS NOT available */
		  // console.log('no support for geolocation');
		}

		var filterList = $('.filterList');
		$('.button').on('click',function(e){
			e.preventDefault();

			if( $(this).hasClass('open') ) {
				// open button
				if( !expanderOpen ) {
					filterList.addClass('open');
					expanderOpen = true;
				} else {
					filterList.removeClass('open');
					expanderOpen = false;
				}
			} else {
				// close button
				if( expanderOpen ) {
					filterList.removeClass('open');
					expanderOpen = false;
				}
			}
		});

		// close radio button filter when user selects a radio button
		$('.filterList li').on('click',function(e){
			e.preventDefault();

			var filterList = $('.filterList'),
				newCaption = $(this).text();
				currentFilter = $(this).attr('data-value');

			if ( !$(this).hasClass('checked') ) {

				$('.filterList li.checked').removeClass('checked');

				$(this).addClass('checked');
				
				$('#' + filterContext + ' .filterList a.open.button').html(newCaption);
				filterList.removeClass('zero').addClass('one');
				$('.filterList').removeClass('error');
			} else {
				$('#' + filterContext + ' .filterList a.open.button').html(newCaption);
				filterList.removeClass('zero').addClass('one');
				console.log(filterContext);
				
			}
			filterList.removeClass('open');
			expanderOpen = false;
		});

		// define map label language/autocomplete language by adding language parameter to google map library link
		// <script src="https://maps.googleapis.com/maps/api/js?key=[API KEY]&amp;libraries=places&amp;language=fr"></script>
		// language=fr

		pngTest = (window.location.href.indexOf('test=png') !== -1);
		mapIcons = buildIcons( (!isIE && !pngTest) ? 'svg' : 'png' );
		// mapIcons = buildIcons( 'png' );

		mainStateController = new HertzStateController({
			"language" : mainLanguage,
			"pages" : ["form","map"],
			"formPage" : $('#form'),
			"formStartField" : "f_startPoint",
			"formEndField" : "f_endPoint",
			"formGoButton" : $('#form a.go'),
			"mapPage" : $('#map'),
			"mapStartField" : "startPoint",
			"mapEndField" : "endPoint",
			"mapGoButton" : $('#map a.go'),
			"mapResetButton" : $('#map a.reset'),
			"mapContainer" : $('#map .map'),
			"mapNode" : "mapbox",
			"sidebarContainer" : $('.results'),
			"sidebar" : $('#poilist'),
			"defaultTypes" : ['art_gallery', 'museum', 'restaurant', 'spa', 'stadium', 'zoo', 'amusement_park', 'park', 'bakery'],
			"brandFilters" : [
				{
					"type" : "global",
					"brands" :["Holiday Inn", "Premier Inn", "Novotel", "Mercure Hotel", "Marriott Hotel", "Travelodge", "DaysInn Hotel", "Holiday Park", "Airport", "Butlins ", "Havens Holidays", "Havens Holiday\'s", "Pontins"]
				},
				{
					"type" : "restaurant",
					"brands" : ["McDonalds", "MC Donnalds ", "Mc Donalds", "McDonald\'s", "Quick", "Subway", "KFC", "Kebab", "Burger King", "Pizza Hut", "Big Fernand", "Nabab", "Paul", "Flunch", "La Mie Caline", "Campaigns", "Wimpy", "Greggs", "Gregg", "Little Chef", "Eat", "Morley\'s Chicken", "Chicken Cottage", "Southern Fried Chicken", "Upper Crust", "West Cornwall Pasty Company", "West Cornwall Pasty Co", "Five Guys", "Costa Coffee", "Café Nero", "Cafe Nero", "Starbucks", "Pret", "Sam\'s Chicken", "Millie\'s Cookies", "Dixy Chicken", "Weatherspoon\'s", "Kebabs", "Nordsee", "Pizza Hut,", "Vapiano", "Doener Kebab", "Doner Kebab", "Sausage shops", "Spizzico", "Kebabs", "Ciao", "Autogrill", "Flunch", "Burger Ranch", "Domino\'s Pizza", "Domincos Pizza", "Wendy\'s", "Papa John\'s Pizza", "Pelicana Chicken", "Tasty Chicken", "Southern Fried Chicken", "Febo", "Panos", "Taco Bell", "Chipstart", "Casa de Kebab", "Patatas Queen", "Takeaway", ]
				},
				{
					"type" : "park",
					"brands" : ['campsite', 'camp site','camping','holiday', 'town hall', 'townhall', 'hotel', 'car park']
				},
				{
					"type" : "bakery",
					"brands" : ['Greggs', 'Gregg\'s', "West Cornwall Pasty Company", "West Cornwall Pasty Co", "West Cornwall Pasty"]
				}
			],
			"searchWindowStartPercent" : 35,
			"searchWindowEndPercent" : 65,
			"searchSegments" : 12,
			"mapIcons" : mapIcons,
			"sidebarIcons" : sidebarIcons,
			"geolocationEnabled" : supportsGelocation,
			"geoPosition" : { "lat" : 0, "lng" : 0 }
		});
	});

	/* ---------------------------------------------------------------------------------- */
	// document ready END
	/* ---------------------------------------------------------------------------------- */

})();