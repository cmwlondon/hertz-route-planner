(function () {
	"use strict";

	/* ------------------------------------------------ */
	// PoiImageLoader
	/* ------------------------------------------------ */
	
	function PoiImageLoader(parameters) {
		this.parameters = parameters;
		this.item = parameters.item;

		this.init();
	}
	
	PoiImageLoader.prototype = {
		"constructor" : PoiImageLoader,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			this.photo = this.item.attr("data-photo");

			// does the point of interest have an image?
			if ( typeof this.photo != 'undefined' ) {

				// yes
		        this.imageTimer = window.setTimeout(function(){
		        	that.imageTimeout();
		        }, 750);

		        try{
					// jquery 2.x
					/*
					var imgLoader = $('<img></img>')
			        .load( function(e){
						console.log('PoiImageLoader 39');
			        	that.imageLoad();
			        })
			        .attr({"src" : this.photo});
					*/

					// jquery 3.x
					var imgLoader = $('<img></img>')
			        .on('load', function(e){
			        	that.imageLoad();
			        })
			        .attr({"src" : this.photo});

					this.item
			        .find('.frameInner').append(imgLoader);
		        }
		        catch(e){
		        }

			} else {
				// no, fade in item
				this.item.removeClass('waitingForImage');
			}
		},

		/* --------------------------------------------------------------------------- */

		"imageLoad" : function () {
			var that = this;

        	this.item
	        .find('.frameInner')
	        .css({"background-image" : "url(" + this.photo + "?time=" + Date.now() + ")"});  

			this.item.removeClass('waitingForImage').addClass("imageLoaded");
			clearTimeout(this.imageTimer);
		},

		/* --------------------------------------------------------------------------- */

		"imageTimeout" : function () {
			var that = this;

			this.item.removeClass('waitingForImage');
		}

		/* --------------------------------------------------------------------------- */
	}

	window.PoiImageLoader = PoiImageLoader;

})();