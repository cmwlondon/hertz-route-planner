(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzStateController
	/* ------------------------------------------------ */

	function HertzStateController(parameters) {
		this.parameters = parameters;

    this.language = parameters.language;

    this.pages = parameters.pages;

    this.formPage = parameters.formPage;
    this.formStartField = parameters.formStartField;
    this.formEndField = parameters.formEndField;
    this.formGoButton = parameters.formGoButton;

    this.mapPage = parameters.mapPage;
    this.mapStartField = parameters.mapStartField;
    this.mapEndField = parameters.mapEndField;
    this.mapGoButton = parameters.mapGoButton;
    this.mapResetButton = parameters.mapResetButton;

    this.mapContainer = parameters.mapContainer;
    this.mapNode = parameters.mapNode;
    this.sidebarContainer = parameters.sidebarContainer;
    this.sidebar = parameters.sidebar;

    this.defaultTypes = parameters.defaultTypes;
    
    this.geolocationEnabled = parameters.geolocationEnabled;
    this.geoPosition = parameters.geoPosition;
    this.searchWindowStartPercent = parameters.searchWindowStartPercent;
    this.searchWindowEndPercent = parameters.searchWindowEndPercent;
    this.searchSegments = parameters.searchSegments;

    this.mapIcons = parameters.mapIcons;
    this.sidebarIcons = parameters.sidebarIcons;

    this.brandFilters = parameters.brandFilters;

    this.currentContext = 'form';
    this.requiredTypes;
    this.foundTypes = {};

    this.startPoint;
    this.startPointLabel = '';
    this.startPointSet = false;
    this.startMarker;

    this.endPoint;
    this.endPointLabel = '';
    this.endPointSet = false;
    this.endMarker;

    this.maxRadius = 0;
    this.maxRadiusPoint = -1;

    this.subset = [];
    this.skipRadii = [];

    this.mapOpen = false;
    this.readyToSearch = false;

		this.setup();
	}
	
	HertzStateController.prototype = {
		"constructor" : HertzStateController,

		"template" : function () { var that = this; },

		"setup" : function () {
			var that = this;

	      this.setupForm({});

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setupForm" : function (parameters) {
	      var that = this;

	      this.formStartAC = this.bindAutocompleter({
	        "textbox" : this.formStartField,
	        "mode" : "start",
	        "context" : "form"
	      });

	      this.formEndAC = this.bindAutocompleter({
	        "textbox" : this.formEndField,
	        "mode" : "end",
	        "context" : "form"
	      });

	      this.formGoButton.on('click', function(e){
	        e.preventDefault();
	        if ( !$(this).hasClass('inactive') ) {
	          that.handleGoButton({});
	        }
	        
	      });
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setupMap" : function (parameters) {
	      var that = this;

	      // console.log("start:'%s' end:'%s'", this.startPointLabel, this.endPointLabel);

	      $('#' + this.mapStartField).val(this.startPointLabel);
	      $('#' + this.mapEndField).val(this.endPointLabel);

	      $('#map .filterList li').each(function(i){
	      	if ( $(this).attr('data-value') == currentFilter ) {
	      		var newCaption = $(this).text();
	      		$(this).addClass('checked');
	      		$('#map .filterList a.open.button').html(newCaption);
	      	}
	      });

	      // $('#map .filterList').removeClass('one').addClass('zero');

	      this.requiredTypes = ( currentFilter != '' ) ? [currentFilter] : [];

	      this.mapGoButton.removeClass('inactive');

	      if ( !TESTMODE ) {
	        this.mapStartAC = this.bindAutocompleter({
	          "textbox" : this.mapStartField,
	          "mode" : "start",
	          "context" : "map"
	        });

	        this.mapEndAC = this.bindAutocompleter({
	          "textbox" : this.mapEndField,
	          "mode" : "end",
	          "context" : "map"
	        });
	      }

	      this.mapGoButton.on('click', function(e){
	        e.preventDefault();
	        if ( !$(this).hasClass('inactive') ) {
	          that.handleGoButton({});
	        }
	        
	      });

	      this.mapResetButton.on('click', function(e){
	        e.preventDefault();
	        if ( !$(this).hasClass('inactive') ) {
	          that.fullReset({});
	        }
	        
	      });

	      // get location from data attributes
	      /*
	      replace with browser/device geolocation, requires https host
	      */
	      var mapCentre = this.setPoint(jQuery('#mapbox').attr('data-centre'));
	      var mapCenterLocation = new google.maps.LatLng( mapCentre.lat, mapCentre.lng);
	      mapOptions.center = mapCenterLocation;

	      // insert map
	      googleMap = new google.maps.Map(document.getElementById( this.mapNode ), mapOptions);

	      // click on map to set start and end points
	      googleMap.addListener('click', function(me) {
	          /*
	          me = google.maps.MouseEvent
	          me.latLng = google.maps.LatLng
	            [ me.latLng.lat(), me.latLng.lng() ]
	          */

	          // first click set start, second sets end
	          // if start marker has been removed, another click will set

	          if ( !that.startMarkerSet ) {
	          	console.log("start lat: %s lng: %s", me.latLng.lat(),me.latLng.lng());

	            that.startMarker = new google.maps.Marker({
	                "position" : me.latLng,
	                "map" : googleMap,
	                "draggable" : false,
	                "icon" : that.mapIcons['start'],
	                "zIndex" : -20
	            });
	            that.startMarkerSet = true;
	            that.startPoint = me.latLng;
	          } else if ( !that.endMarkerSet ) {
	          	console.log("end lat: %s lng: %s", me.latLng.lat(),me.latLng.lng());

	            that.endMarker = new google.maps.Marker({
	                "position" : me.latLng,
	                "map" : googleMap,
	                "draggable" : false,
	                "icon" : that.mapIcons['end'],
	                "zIndex" : -20
	            });
	            that.endMarkerSet = true;
	            that.endPoint = me.latLng;
	          }

	          if (that.startMarkerSet && that.endMarkerSet) {
	            that.mapGoButton.removeClass('inactive');            
	          }

	          me.stop();
	        });

	      // resize map if browser viewport resizes
	      $(window).resize(function(){
	      google.maps.event.trigger(googleMap, "resize");

	        if( that.startMarkerSet && that.endMarkerSet ) {
	          var routeBounds = new google.maps.LatLngBounds();
	          routeBounds.extend(that.startPoint);
	          routeBounds.extend(that.endPoint);
	          googleMap.fitBounds(routeBounds, 0);
	        }
	      });

	      this.directionsService = new google.maps.DirectionsService;
	      this.directionsDisplay = new google.maps.DirectionsRenderer({ "suppressMarkers" : true });
	      // this.directionsDisplay.setMap(googleMap);

	      placesService = new google.maps.places.PlacesService(googleMap);

	      this.SideBarModule = new HertzPlacesSidebar({
	        "language" : this.language,
	        "list" : $('div.results ul#poilist'),
	        "controls" : $('div.results a.arrows'),
	        "itemsPerPage" : 3,
	        "itemHeight" : 242,
	        "pageHeight" : (242 + 3) * 3
	      });

	      this.POIList = new HertzPlaces({});

	      this.mapOpen = true;
	      this.currentContext = 'map';      
	      filterContext = 'map';
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "bindAutocompleter" : function (parameters) {
	      var that = this,
	          newACtemp;

	      newACtemp = new google.maps.places.Autocomplete( (document.getElementById(parameters.textbox)), { "types" : ['geocode'], "language" : this.language});
	      console.log(this.language);
	      newACtemp.addListener('place_changed', function() {

	        try{
	          that.placeChange({
	            "ac" : this,
	            "mode" : parameters.mode,
	            "context" : parameters.context
	          });
	        }

	        catch (e) {
	          console.log(e);
	        }

	      });

	      return newACtemp;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setPoint" : function (pointString) {
	      var that = this,
	      pt = pointString.split(",");

	      return {"lat" : pt[0], "lng" : pt[1] };
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "setRoutePoint" : function (parameters) {
	      var that = this;

	      switch( parameters.place ) {
	        case "start" : {
	          this.startPoint = new google.maps.LatLng( parameters.lat, parameters.lng );
	          this.startPointLabel = parameters.label;
	          this.startMarkerSet = true;
	        } break;
	        case "end" : {
	          this.endPoint = new google.maps.LatLng( parameters.lat, parameters.lng );
	          this.endPointLabel = parameters.label;
	          this.endMarkerSet = true;
	        } break;
	      }

	      if (this.startMarkerSet && this.endMarkerSet) {
	        switch( this.currentContext ) {
	          case "form" : {
	            this.formGoButton.removeClass('inactive');
	          } break;
	          case "map" : {

	            this.mapGoButton.removeClass('inactive');
	          } break;
	        }
	      }

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "placeChange" : function (parameters) {
	      var that = this,
	          newPlace = parameters.ac.getPlace(),
	          newPlaceLatLng;

	          if(newPlace !== null && typeof newPlace === "object") {
	              newPlaceLatLng = newPlace.geometry.location;

	              switch( parameters.mode ) {
	                case "start" : {
	                  this.startPoint = newPlaceLatLng;
	                  this.startPointLabel = newPlace.formatted_address;
	                  this.startMarkerSet = true;
	                } break;
	                case "end" : {
	                  this.endPoint = newPlaceLatLng;
	                  this.endPointLabel = newPlace.formatted_address;
	                  this.endMarkerSet = true;
	                } break;
	              }

	              if (this.startMarkerSet && this.endMarkerSet) {
	                switch( parameters.context ) {
	                  case "form" : {
	                    this.formGoButton.removeClass('inactive');
	                  } break;
	                  case "map" : {

	                    this.mapGoButton.removeClass('inactive');
	                  } break;
	                }
	              }
	          }  
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "handleGoButton" : function (parameters) {
	      var that = this,
	          filtersSelected = false;

	      // form context - switch to map, draw start, end, route and do search
	      // map context - draw start, end, route and do search

	      if ( this.mapOpen ) {
	        this.resetMap();
	      }
	      
	      this.sidebarContainer.removeClass('noroute').removeClass('nopoi').removeClass('notype').removeClass('initial').addClass('searching');

	      this.requiredTypes = ( currentFilter != '' ) ? [currentFilter] : [];
	      console.log(this.requiredTypes);
			/*
			this.requiredTypes = this.getSelectedFilters({
				"ids" : ( this.currentContext == 'form' ) ? this.formFilters : this.mapFilters
			});
			*/

	      filtersSelected = ( this.requiredTypes.length > 0);

	      switch ( this.currentContext ) {
	        case "form" : {

	          if ( filtersSelected ) {
	            this.showMap({});
	          } else {
	            // no filters selected, display error
	            that.mapError({"error" : "notype"});
	          }
	        } break;
	        case "map" : {

	          if ( !filtersSelected ) {
	            // no filters selected, display error
	            that.mapError({"error" : "notype"});
	          }

	        } break;
	      }

	      if ( filtersSelected ) {
	        if ( !TESTMODE ) {
				this.mapGoButton.addClass('inactive');
				this.getRoute({});
	        }
	        
	      }
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showMap" : function (parameters) {
	      var that = this;

	      this.formPage.hide();
	      this.mapPage.show();
	      this.setupMap({});
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "fullReset" : function (parameters) {
	      var that = this;

	      this.resetMap({});

	      // clear star and end markers
	      this.startPoint = null;
	      this.startPointLabel = '';
	      this.startMarkerSet = false;
	      this.endPoint = null;
	      this.endPointLabel = '';
	      this.endMarkerSet = false;

	      if ( !TESTMODE ) {
	        this.mapStartAC.set('place', null);
	        this.mapEndAC.set('place', null);

	      }

	      $( '#' + this.mapStartField ).val('');
	      $( '#' + this.mapEndField ).val('');

	      // reset category selection to all tyoes
	      $('#map .filterList input[type=checkbox]').each(function(i){
	          $(this).prop('checked', true);
	      });

	      this.sidebarContainer.removeClass('noroute').removeClass('nopoi').removeClass('notype').removeClass('searching').addClass('initial');
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "resetMap" : function (parameters) {
	      var that = this;

	      if ( this.startMarker ) {
	        this.startMarker.setMap(null);
	      }
	      
	      if ( this.endMarker ) {
	        this.endMarker.setMap(null);
	      }

	      if( this.directionsDisplay) {
	        this.directionsDisplay.setMap(null);
	      }

	      if ( poiMarkers.length > 0) {
	        var pmi = 0,
	            pmc = poiMarkers.length,
	            tpm;

	        do {
	          tpm = poiMarkers[pmi];

	          tpm.setMap(null);

	          pmi++;
	        } while (pmi < pmc)
	      }

	      // radii
	      if ( radii.length > 0 ) {
	        var ri = 0,
	            rc = radii.length,
	            tr;

	        do {
	          tr = radii[ri];

	          tr.setMap(null);

	          ri++;
	        } while (ri < rc)
	      }

	      this.originalPath = [];
	      this.workingPath = [];
	      this.subset = [];
	      this.foundTypes = {};

	      if ( this.POIList ) {
	        this.POIList.empty();
	      }
	      
	      if ( this.SideBarModule ) {
	        this.SideBarModule.clear();  
	      }
	      
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "getRoute" : function (parameters) {
	      var that = this;

	      this.directionsService.route(
	        {
	          "origin" : this.startPoint,
	          "destination" : this.endPoint,
	          "travelMode" : 'DRIVING',
	          "language" : this.language
	        },
	        function(response, status) {
	          if (status === 'OK') {

	            that.routeLength = response.routes[0].legs[0].distance.value;
	            that.routeSteps = response.routes[0].legs[0].steps.length;

	            console.log("this.routeLength: %skm this.routeSteps: %s", (that.routeLength / 1000), that.routeSteps);

	            that.readyToSearch = false;

	            that.drawRouteMarkers({});
	            that.drawRoute({"response" : response});
	            that.getSearchPoints({"response" : response});

	            /* */
	            if ( that.readyToSearch ) {
	            	that.buildPOISearchQueue({});	
	            }
	            /* */

	            /*
	            var routeBoxer = new RouteBoxer();
	            var path = response.routes[0].overview_path;
          		var boxes = routeBoxer.box(path, 5000);
          		console.log(boxes);
	            */

	          } else {
	            // inability to find route
	            that.mapError({"error" : "noroute"});
	          }
	        }
	      );

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "mapError" : function (parameters) {
	      var that = this;

	      switch ( parameters.error ) {
	        case "noroute" : {
	          console.log('ERROR: no route found');
	          this.sidebarContainer.removeClass('searching').addClass('noroute');
	        } break;
	        case "nopoi" : {
	          console.log('ERROR: no points of interest found');
	          this.sidebarContainer.removeClass('searching').addClass('nopoi');
	          this.mapGoButton.removeClass('inactive');
	        } break;
	        case "notype" : {
	          console.log('ERROR: no POI categories selected');

	          switch ( this.currentContext ) {
	            case "form" : {
	              // display filter error popup/message in form
	              $('.filterList').addClass('error');
	            } break;
	            case "map" : {
	              // display filter error in sidebar
	              this.sidebarContainer.removeClass('searching').addClass('notype');
	            } break;
	          }
	        } break;
	      }
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "drawRouteMarkers" : function (parameters) {
	      var that = this,
	          routeBounds;

	      this.startMarker = new google.maps.Marker({
			// "animation" : google.maps.Animation.BOUNCE,
			"position" : this.startPoint,
			"map" : googleMap,
			"draggable" : false,
			"icon" : this.mapIcons['start'],
			"zIndex" : -20,
			"optimized" : false
	      });

	      this.endMarker = new google.maps.Marker({
			// "animation" : google.maps.Animation.BOUNCE,
			"position" : this.endPoint,
			"map" : googleMap,
			"draggable" : false,
			"icon" : this.mapIcons['end'],
			"zIndex" : -20,
			"optimized" : false
	      });

	      // centre map on route
	      routeBounds = new google.maps.LatLngBounds();
	      routeBounds.extend(this.startPoint);
	      routeBounds.extend(this.endPoint);
	      googleMap.fitBounds(routeBounds, 0);
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "drawRoute" : function (parameters) {
	      var that = this;

	      this.directionsDisplay.setMap(googleMap);
	      this.directionsDisplay.setDirections(parameters.response);
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "getSearchPoints" : function (parameters) {
	      var that = this;

	      // parameters.response
	      // parameters.response.routes[0].overview_path

	      /*
	      this.originalPath, this.workingPath
	      [ { "lat" : LAT, "lng" : LNG }, { "lat" : LAT, "lng" : LNG }, ... ]
	      */

	      this.originalPath = this.storePathPoints(parameters.response.routes[0].overview_path);
	      this.workingPath = this.cloneArray(this.originalPath);

	      // this.savePOIList(this.originalPath, 'originalpath.json');

	      // this.trimPath();
	      // this.trimAreas();

	      this.segmentPath({
	      	"path" : parameters.response.routes[0].overview_path,
	      	"segments" : this.searchSegments
	      })
	      // this.showSearchAreas();
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "segmentPath" : function (parameters) {
			var that = this;
			/*
			in:
			parameters = {
				"path" : [{lat,lng}], // array of points = response.routes[0].overview_path
				"segments" : 12
			}
			out:
			this.subset
			*/

			var previousPoint = this.startPoint,
				ptpDistance = 0,
				totalDistance = 0;

			// calculate total route distance and set distance from start on each point in path
			parameters.path.map(function(point,index){
				ptpDistance = google.maps.geometry.spherical.computeDistanceBetween(previousPoint, point);
				totalDistance = totalDistance + ptpDistance;
				point.ptp = ptpDistance;
				point.pd = totalDistance;
				previousPoint = point;
			});

			// find beginning and end of search window, from 25% to 75% of total route
			var windowStartIndex = -1,
				windowEndIndex = -1,
				windowStartDistance = totalDistance * (this.searchWindowStartPercent / 100),
				windowEndDistance = totalDistance * (this.searchWindowEndPercent / 100),
				windowDistance = windowEndDistance - windowStartDistance;

			parameters.path.map(function(point,index){
				if (windowStartIndex == -1 && point.pd > windowStartDistance) {
					windowStartIndex = index;
				}
				if (windowEndIndex == -1 && point.pd > windowEndDistance) {
					windowEndIndex = index;
				}
			});

			// plot start and end of window
			var windowStart = parameters.path[windowStartIndex],
				windowEnd = parameters.path[windowEndIndex];

			// trim path down to window
			var spliceCount = windowEndIndex - windowStartIndex;
			var windowPath = parameters.path.splice(windowStartIndex, spliceCount);

			var segmentLength = windowDistance / parameters.segments,
				segmentNodes = [],
				sd = windowStart.pd,
				wd = 0;

			segmentNodes.push(windowStart);
			windowPath.map(function(point,index){

				wd = point.pd - sd;
				if ( wd > segmentLength ) {
					segmentNodes.push(point);
					sd = sd + wd;
				}
			});

			/* */
	        // console.log("length of window: %s", windowDistance);
			/* */

			segmentNodes.map(function(point){
				point.pr = segmentLength;
			});
	        this.subset = segmentNodes;
	        this.readyToSearch = true;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "rhumbDestinationPoint" : function (parameters) {
			var that = this;

			/*
			parameters = {
				"point" : LatLng
				"bearing" : 0,
				"distance" : 500
			}
			*/

			var R = 6371, // earth's mean radius in km
				d = parseFloat(parameters.distance) / R,  // d = angular distance covered on earth's surface
				lat1,
				lon1,
				bearing;

			bearing = parameters.bearing.toRad();
			lat1 = parameters.point.lat().toRad();
			lon1 = parameters.point.lng().toRad();

			var lat2 = lat1 + d * Math.cos(bearing);
			var dLat = lat2 - lat1;
			var dPhi = Math.log(Math.tan(lat2 / 2 + Math.PI / 4) / Math.tan(lat1 / 2 + Math.PI / 4));
			var q = (Math.abs(dLat) > 1e-10) ? dLat / dPhi : Math.cos(lat1);
			var dLon = d * Math.sin(bearing) / q;
			// check for going past the pole
			if (Math.abs(lat2) > Math.PI / 2) {
				lat2 = lat2 > 0 ? Math.PI - lat2 : - (Math.PI - lat2);
			}
			var lon2 = (lon1 + dLon + Math.PI) % (2 * Math.PI) - Math.PI;

			if (isNaN(lat2) || isNaN(lon2)) {
				return null;
			}
			return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "trimPath" : function (parameters) {var that = this;},

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "trimAreas" : function (parameters) {var that = this;},

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildPOISearchQueue" : function (parameters) {
	      var that = this,
	          superThat = this;

	      var filters = (this.requiredTypes.length == 1) ? this.requiredTypes : [];
	      var workingSubset = this.cloneArray(this.subset);
	      
	      var queueParameters = {
	        "queue" : workingSubset, // create clone of subset array
	        "worker" : {
	          "global" :{
	            "language" : this.language,
	            "radius" : 500, // 500 metres
	            "type" : filters
	          },
	          "action" : function(){
	            var that = this,
	                pointLatLng;
	            var waiting = window.setTimeout(function(){

					pointLatLng = new google.maps.LatLng( that.settings.item.lat(), that.settings.item.lng() );

					superThat.showSearchArea({
						"point" : pointLatLng,
						"radius" : that.settings.item.pr
					});

					placesService.nearbySearch({
						"location" : pointLatLng,
						"radius" : that.settings.item.pr,
						// "bounds" : that.settings.item,
						"type" : that.settings.global.type,
						"language" : that.settings.global.language
					},
					function( results, status ){

						if ( status == 'OK' ) {
							var ri = 0,
							rc = results.length,
							tr,
							trTypeStatus;

							do {
								tr = results[ri];

								// unrated places go to end of list
								if (!tr.hasOwnProperty('rating')) {
									tr.rating = 0;
								}

								tr.xd = pointLatLng;
								tr.sd = google.maps.geometry.spherical.computeDistanceBetween(pointLatLng,tr.geometry.location);
								// capture POI if it matches one of the required types
								// trTypeStatus = testTypes(tr.types, requiredTypes);

								// store POI if it matches one of default types
								trTypeStatus = superThat.testFilters({
									"itemTypes" : tr.types,
									"typepool" : superThat.defaultTypes
								});
								tr.group = trTypeStatus.type;
								tr.distance = that.settings.item.pd; // store distance from start of route in POI data
								if ( trTypeStatus.isMatch ) {
									superThat.POIList.addPlace(tr);  
								}

								ri++;
							} while ( ri < rc )
						}              
						that.completed();

					});
	            }, 500); // limit requests per second to prevent exceeding request per second quota

	          },
	          "callback" : function(){
	            this.queue.workerCompleted();
	          }
	        },
	        "finished" :  function(){
	            var poiCount = superThat.POIList.getCount();
	            console.log( "POI found: %s", poiCount );

	            if ( poiCount > 0 ) {
	              superThat.showPOIs();
	            } else {
	              superThat.mapError({"error" : "nopoi"});
	            }          
	        }
	      };
	      this.queue = new Queue(queueParameters);
	      this.queue.startWorker();

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showPOIs" : function (parameters) {
			var that = this;

			this.sidebarContainer.removeClass('searching').addClass('blank');

			var pi = 0,
				pc,
				allPOI,
				tp,
				workingPOI = [];

			this.foundTypes = this.POIList.typeFilter(this.requiredTypes);
			this.POIList.brandFilter({
				"requiredTypes" : this.requiredTypes,
				"filters" : this.brandFilters
			});
			this.POIList.ratingSort();

			workingPOI = this.POIList.getPlaces();

			if ( workingPOI.length > 0 ) {
				this.buildPOIMarkers({"items" : workingPOI});
				this.buildSidebar({"items" : workingPOI});

				var wait = window.setTimeout(function() {

					// three cases
					// N % 3 == 0
					// last item is third item

					// mark every third entry - open info panel in different manner for last itme in page
					that.sidebar.find('li.entry').not('li.template').each(function(i){
					if ( (i + 1) % 3 == 0 ) {
					  $(this).addClass('third');
					}
					});

					that.sidebar.find('li.entry').not('li.template').eq(that.sidebar.find('li.entry').not('li.template').length - 1).addClass('last');

					// update sidebor controls
					that.SideBarModule.configure();
					that.SideBarModule.pageImageLoad(0,0);
					that.mapGoButton.removeClass('inactive');

				},100);
			} else {
				console.log('x');
				this.mapError({"error" : "nopoi"});
			}

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildPOIMarkers" : function (parameters) {
	      var that = this;

	      var pi = 0, pc = parameters.items.length,tp;

	      do {
	        tp = parameters.items[pi];

	        this.buildPOIMarker({
	          "latlng" : tp.geometry.location,
	          "icon" : this.mapIcons[tp.group],
	          "name" : tp.name,
	          "type" : tp.group,
	          "place_id" : tp.place_id 
	        });

	        pi++;
	      } while (pi < pc)
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildPOIMarker" : function (parameters) {
	      var that = this;

	      var newMarker = new google.maps.Marker({
	        "icon" : parameters.icon,
	        "position" : parameters.latlng,
	        "map" : googleMap,
	        "draggable" : false,
	        "zIndex" : -20,
	        "place_name" : parameters.name,
	        "place_id" : parameters.place_id,
	        "icontype" : parameters.type
	      })

	      newMarker.addListener('click', function(poiMarker) {
	        var clickName = this.place_name,
	            clickid = this.place_id,
	            clickLatLng = this.position,
	            clickedpoi,
	            clickedpoiIndex;

	        // console.log("POI marker: '%s' place_id: %s", clickName, clickid);
	        // identify where in the results list this POI is and scroll to it
	        clickedpoi = $('#poilist li[data-place-id="' + clickid + '"]');
	        // clickedpoi.find('.frame').addClass('shake');
	        clickedpoiIndex = $('#poilist li.entry').not('li.template').index(clickedpoi);

	        // scroll to page in sidebar
	        that.SideBarModule.mapEvent(clickedpoiIndex,clickid);

	        googleMap.panTo( clickLatLng );
	        if (zoomLevel != -1) {
	        	googleMap.setZoom(zoomLevel);	
	        }

	      });

	      poiMarkers.push(newMarker);
	      poiMarkerIndex.push({
	      	"index" : poiMarkers.length -1,
	      	"place_id" : parameters.place_id
	      });

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "buildSidebar" : function (parameters) {
	      var that = this;

	      var pi = 0, pc = parameters.items.length,tp,tpLatLng;

	      do {
	        tp = parameters.items[pi];

	        this.SideBarModule.addEntry( new HertzPlacesSidebarItem({
	          "sidebar" : this.SideBarModule,
	          "list" : this.sidebar,
	          "item" : tp
	        }) );
	        
	        pi++;
	      } while (pi < pc)

	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showSearchAreas" : function (parameters) {
	      var that = this,
	          si = 0,
	          sc = this.subset.length,
	          ts,
	          tsLatLng;

	      do {
	        ts = this.subset[si];
	        tsLatLng = new google.maps.LatLng( ts.lat, ts.lng );

	        this.showSearchArea({
	          "point" : tsLatLng,
	          "radius" : ts.pr
	        });

	        si++;
	      } while ( si < sc )
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "showSearchArea" : function (parameters) {
	      var that = this;

	      radii.push(new google.maps.Circle({
	        "strokeColor" : HertzYellow,
	        "strokeOpacity" : 0.8,
	        "strokeWeight" : 2,
	        "fillColor" : HertzYellow,
	        "fillOpacity" : 0.35,
	        "map" : googleMap,
	        "center" : parameters.point,
	        "radius" : parameters.radius // radius in metres
	      }));
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "getSelectedFilters" : function (parameters) {
	      var that = this,
	          filterIndex = 0,
	          thisFilter,
	          foundTypes = [];

	      do {
	        thisFilter = $(parameters.ids[filterIndex]);

	        if ( thisFilter.is(':checked') ) {
	          foundTypes.push(thisFilter.val());
	        }
	        filterIndex++;
	      } while ( filterIndex < parameters.ids.length )

	      return foundTypes;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "testFilters" : function (parameters) {
	      /*
	      parameters.itemTypes
	      parameters.typepool
	      */
	      var that = this,
	          rtIndex = 0,
	          rtCount = parameters.typepool.length,
	          rtItem,
	          result = {
	            "isMatch" : false,
	            "type" : "unknown"
	          };

	      do {
	        rtItem = parameters.typepool[rtIndex];

	        if ( parameters.itemTypes.indexOf(rtItem) != -1 ) {
	          result = {
	            "isMatch" : true,
	            "type" : rtItem 
	          };
	          break;
	        }
	        rtIndex++;
	      } while ( rtIndex < rtCount) 

	      return result;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "storePathPoints" : function (path) {
	      var that = this,
	          pi = 0,
	          pc = path.length,
	          tp,
	          newPath = [];

	      do {
	        tp = path[pi];

	        newPath.push({
	          "lat" : tp.lat(),
	          "lng" : tp.lng()
	        });

	        pi++;
	      } while (pi < pc)

	      return newPath;
		}, 

		/* --------------------------------------------------------------------------- */

		"quickSort" : function  (arr, left, right) {
			var i = left,
				j = right,
				tmp,
				pivotidx = (left + right) / 2,
				pivot = arr[pivotidx.toFixed()];  

			/* partition */
			while (i <= j)
			{
				// ascending order
				while (arr[i] < pivot) {
					i++;
				}

				while (arr[j] > pivot) {
					j--;
				}

				// descending order
				/*
				while (arr[i] > pivot) {
					i++;
				}

				while (arr[j] < pivot) {
					j--;
				}
				*/

				if (i <= j)
				{
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;

					i++;
					j--;
				}
			}

			/* recursion */
			if (left < j)
				this.quickSort(arr, left, j);
			if (i < right)
				this.quickSort(arr, i, right);
			return arr;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "cloneArray" : function (array) {
	      var that = this,
	          ai = 0,
	          ac = array.length,
	          ta,
	          newArray = [],
	          newObject;

	      do {
	        ta = array[ai];

	        newObject = {};

	        Object.keys(ta).map(function(property){
	        	newObject[property] = ta[property];
	        });
	        newArray.push(newObject);

			/* */

	        ai++;
	      } while (ai < ac)

	      return newArray;
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "savePOIList" : function (variable,filename) {
	      var that = this;

	      var hiddenElement = document.createElement('a');
	      hiddenElement.href = 'data:attachment/text,' + encodeURI( JSON.stringify( variable ) );
	      hiddenElement.target = '_blank';
	      hiddenElement.download = filename;
	      hiddenElement.click();
	    },

	    /* ---------------------------------------------------------------------------------------------------------------- */

	    "testSearch" : function (parameters) {
			var that = this;

			// parameters.latlng
			placesService.nearbySearch({
				"location" : parameters.latlng,
				"radius" : 5000,
				// "bounds" : that.settings.item,
				"type" : ['natural_feature'],
				"language" : "en"
			}, function( results, status ){

				googleMap.panTo(parameters.latlng);

				var newMarker = new google.maps.Marker({
					"position" : parameters.latlng,
					"map" : googleMap,
					"draggable" : false,
					"zIndex" : -20
				});

				radii.push(new google.maps.Circle({
					"strokeColor" : HertzYellow,
					"strokeOpacity" : 0.8,
					"strokeWeight" : 2,
					"fillColor" : HertzYellow,
					"fillOpacity" : 0.35,
					"map" : googleMap,
					"center" : parameters.latlng,
					"radius" : 5000 // radius in metres
				}));

				// that.savePOIList(results, 'margate.json');
			});


		}

		/* ---------------------------------------------------------------------------------------------------------------- */
	}

	window.HertzStateController = HertzStateController;

})();