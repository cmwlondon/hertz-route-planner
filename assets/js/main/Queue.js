(function () {
	"use strict";

	/* ------------------------------------------------ */
	// Queue
	/* ------------------------------------------------ */
	
	function Queue(parameters) {
		this.parameters = parameters;
		this.queue = parameters.queue;
		this.worker = parameters.worker;
		this.finished = parameters.finished;
	
		this.queueLimit = this.queue.length;
		this.workerBox = [];
		this.workerRunning = false;

		this.init();
	}
	
	Queue.prototype = {
		"constructor" : Queue,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

		},
		"startWorker" : function () {
			var that = this,
				newItem,
				newWorker,
				waiting = [],
				waiting2 = [];


			if ( this.queueLimit > 0 ) {
				// newItem = this.queue.pop(); // get LAST element in queue
				newItem = this.queue.splice(0,1); // get FIRST item in queue

				this.queueLimit = this.queue.length;

				this.workerRunning = true;

				newWorker = new Worker({
					"queue" : this,
					"settings" : {
						"global" : that.worker.global,
						"item" : newItem[0]
					},
					"action" : that.worker.action,
					"callback" : that.worker.callback
				});
			}
		},
		"workerCompleted" : function () {
			var that = this;

			if ( this.queueLimit > 0 ) {
				this.startWorker();
			}

			if ( !this.workerRunning) {
				this.queueCompleted();
			}

			if ( this.queueLimit == 0 && this.workerRunning) {
				this.workerRunning = false;
			}


		},
		"queueCompleted" : function () {
			var that = this;

			this.finished();
		}
	};

	window.Queue = Queue;
})();
