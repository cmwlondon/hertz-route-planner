(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzPlacesSidebar
	/* ------------------------------------------------ */
	
	function HertzPlacesSidebar(parameters) {
		this.parameters = parameters;

		this.language = parameters.language;

		this.list = parameters.list;
		this.itemsPerPage = parameters.itemsPerPage;
		this.itemHeight = parameters.itemHeight;
		this.pageHeight = parameters.pageHeight;
		this.controls = parameters.controls;

		this.upButton;
		this.downButton;
		this.items = [];
		this.entries;
		this.count = 0;

		this.pages = 0;
		this.currentPage = 0;
		this.listOffset = 0;

		this.isOpen;
		this.openEntryIndex = -1;
		this.enigma = 0;
		this.bounceMarkerIndex = -1;
		this.bounceMarkerId = '';
		this.imageLoaders = [];

		this.thirdSpeed = 250;

		this.init();
	}
	
	HertzPlacesSidebar.prototype = {
		"constructor" : HertzPlacesSidebar,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			this.pageoffset = this.pageHeight;

			this.controls.each(function(i){
				if ( jQuery(this).hasClass('up') ) {
					that.upButton = jQuery(this);
				} else {
					that.downButton = jQuery(this);
				}
			});

			this.controls.on('click', function (e) {
				e.preventDefault();

				if (jQuery(this).hasClass('up')) {
					that.pageUp();
				} else {
					that.pageDown();
				}
			});
		},

		/* --------------------------------------------------------------------------- */

		"configure" : function () {
			var that = this;

			this.update();

		},

		/* --------------------------------------------------------------------------- */
		
		"update" : function () {
			var that = this;

			this.entries = this.list.find('li.entry').not('li.template');
			this.count = this.entries.length;

			this.pages = Math.floor(this.count / this.itemsPerPage);
			this.pages = (this.count % this.itemsPerPage > 0) ? this.pages + 1 : this.pages;
			this.currentPage = 0;

			this.updateControls();
		},

		/* --------------------------------------------------------------------------- */
		
		"pageUp" : function () {
			var that = this;

			if ( this.currentPage > 0 ) {
				this.currentPage = this.currentPage - 1;
				this.pageJump(this.currentPage);
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"pageDown" : function () {
			var that = this;

			if ( this.currentPage < (this.pages - 1) ) {
				this.currentPage = this.currentPage + 1;
				this.pageJump(this.currentPage);
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"pageJump" : function (pageIndex) {
			var that = this;

			this.enigma = this.count % 3;

			this.updateControls();

			this.listOffset = this.pageoffset * -pageIndex;

			if ( pageIndex == (this.pages - 1) && this.pages > 1 ) {
				if ( this.enigma != 0 ) {

					switch ( this.enigma ) {
						case 1 : {
							// N % 3 == 1
							// 1 item in last page, display 2nd and 3rd items from previous page, remove 'third' class, add 'last'  class to last item
							this.listOffset = this.listOffset + (this.itemHeight * 2) + 3;
						} break;
						case 2 : {
							// N % 3 == 2
							// 2 items in last page, display 3rd items from previous page, remove 'third' class, add 'last'  class to last item
							this.listOffset = this.listOffset + this.itemHeight + 3;
						} break;
					}				
				}
			} else {
				this.enigma = 0;
			}

			// find items on selected page and load images
			this.pageImageLoad( pageIndex, this.enigma );
			this.list.css({"top" : this.listOffset + "px"});
		},

		/* --------------------------------------------------------------------------- */
		
		"itemReset" : function (itemIndex) {
			var that = this;

			this.list.find('.third,.last').animate({"top" : "0px"}, this.thirdSpeed, function(){
				$(this).css({"z-index" : ""});
			});
		},

		/* --------------------------------------------------------------------------- */
		
		"itemJump" : function (itemIndex) {
			var that = this;

			// handle opening / closing item info panel, plus map focus
			// convert item index to page index
			// direct to item -> newListOffset = poiIndex * -itemheight;
			// page -> newListOffset = (-itemheight * itemsperpage) * Math.floor(poiIndex / itemsperpage);
			this.currentPage = Math.floor(itemIndex / 3);
			this.pageJump( this.currentPage );
		},

		/* --------------------------------------------------------------------------- */
		
		"hideControls" : function () {
			var that = this;

			// this.controls.hide();
		},

		/* --------------------------------------------------------------------------- */
		
		"updateControls" : function () {
			var that = this;

			$('.results').removeClass('noarrows');

			if ( this.currentPage == 0) {
				this.upButton.hide();
			} else {
				this.upButton.show();
			}

			if ( this.currentPage == (this.pages - 1) ) {
				this.downButton.hide();
			} else {
				this.downButton.show();
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"addEntry" : function (entry) {
			var that = this;

			this.items.push(entry);
		},

		/* --------------------------------------------------------------------------- */
		
		"openEntry" : function (clickIndex) {
			var that = this,
				thisEntry = this.entries.eq(clickIndex),
				thisPlaceID = thisEntry.attr('data-place-id'),
				metaBlock = thisEntry.find('.meta'),
				metaSet = metaBlock.attr('data-meta');

			if ( TESTMODE ) { metaSet == 1; }

			if (metaSet == 1) {

				if ( this.currentPage == (this.pages - 1) && this.enigma != 0) {
					if ( thisEntry.hasClass('last') ) {
						// thisEntry.css({"top" : "-231px", "z-index" : "40"});
						thisEntry
						.css({"z-index" : "40"})
						.animate({"top" : "-245px"}, this.thirdSpeed, function(){});
					}
				} else {
					if ( thisEntry.hasClass('third') ) {
						// thisEntry.css({"top" : "-231px", "z-index" : "40"});
						thisEntry
						.css({"z-index" : "40"})
						.animate({"top" : "-245px"}, this.thirdSpeed, function(){});
					}
				}

				thisEntry.addClass('open');
				this.isOpen = true;
				this.openEntryIndex = clickIndex;

				// this.upButton.hide();
				// this.downButton.hide();
				$('.results').addClass('noarrows');

			} else {
				$('.results').addClass('noarrows');
				placesService.getDetails({"placeId" : thisPlaceID, "language" : this.language}, function (place, status) {
					if (status == 'OK') {
			            metaBlock.attr({ "data-meta" : "1",  "data-url" : place.url, "data-web" : place.website});

			            if ( place.hasOwnProperty('website') ) {
				            if ( place.website != '' ) {
								thisEntry.find('p.web a').attr({"href" : place.website});
				            } else {
				            	thisEntry.find('p.web').hide();
				            }
			            } else {
			            	thisEntry.find('p.web').hide();
			            }

						if ( that.currentPage == (that.pages - 1) && that.enigma != 0) {
							if ( thisEntry.hasClass('last') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						} else {
							if ( thisEntry.hasClass('third') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						}

						// phone number
						// place.formatted_phone_number
						// place.international_phone_number
						if ( place.hasOwnProperty('international_phone_number') ) {
							if ( place.international_phone_number != '') {
								thisEntry.find('p.phone span').text(place.international_phone_number);
							} else {
								thisEntry.find('p.phone').hide();
							}
						} else {
							thisEntry.find('p.phone').hide();
						}

						// opening times
						// places.opening_hours.weekday_text = []
						if ( place.opening_hours ) {
							if (place.opening_hours.weekday_text) {

								var times = place.opening_hours.weekday_text,
									out = thisEntry.find('div.ot ul'),
									newline,
									ti = 0,
									tc = times.length,
									tt;

								out.empty();
								do {
									tt = times[ti];

									newline = jQuery('<li></li>').text(tt);
									out.append(newline);

									ti++;
								} while (ti < tc)
								thisEntry.removeClass('noOpeningTimes');
							} else {
								thisEntry.addClass('noOpeningTimes');
								thisEntry.find('div.ot').hide();
							}
						} else {
							thisEntry.addClass('noOpeningTimes');
							thisEntry.find('div.ot').hide();
						}

						if ( that.currentPage == (that.pages - 1) && that.enigma != 0) {
							if ( thisEntry.hasClass('last') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						} else {
							if ( thisEntry.hasClass('third') ) {
								// thisEntry.css({"top" : "-231px", "z-index" : "40"});
								thisEntry
								.css({"z-index" : "40"})
								.animate({"top" : "-245px"}, that.thirdSpeed, function(){});
							}
						}

						thisEntry.addClass('open');
						that.isOpen = true;
						that.openEntryIndex = clickIndex;

						$('.results').addClass('noarrows');

						// that.upButton.hide();
						// that.downButton.hide();

		        	}
				});
			}

		},

		/* --------------------------------------------------------------------------- */
		
		"closeEntry" : function () {
			var that = this,
				thisEntry = this.entries.eq(this.openEntryIndex);

			thisEntry
			.animate({"top" : "0px"}, this.thirdSpeed, function(){
				thisEntry.css({"z-index" : ""});
			});

			/*
			if ( this.currentPage == (this.pages - 1) && this.enigma != 0) {
				if ( thisEntry.hasClass('last') ) {
					// thisEntry.css({"top" : "-231px", "z-index" : "40"});
					thisEntry
					.animate({"top" : "0px"}, this.thirdSpeed, function(){
						thisEntry.css({"z-index" : ""});
					});
				}
			} else {
				if ( thisEntry.hasClass('third') ) {
					// thisEntry.css({"top" : "-231px", "z-index" : "40"});
					thisEntry
					.animate({"top" : "0px"}, this.thirdSpeed, function(){
						thisEntry.css({"z-index" : ""});
					});
				}
			}
			*/

			thisEntry.removeClass('open');
			this.isOpen = false;
			this.openEntryIndex = -1;
			this.updateControls();
			$('.results').removeClass('noarrows');
		},

		/* --------------------------------------------------------------------------- */
		
		"mapJump" : function (clickIndex) {
			var that = this,
			thisEntry = this.entries.eq(clickIndex),
	        poiLocation = new google.maps.LatLng( thisEntry.attr('data-lat'), thisEntry.attr('data-lng')),
	        poiPlaceId = thisEntry.attr('data-place-id');

	        this.markerChange(poiPlaceId);

	        googleMap.panTo( poiLocation ); 
	        if (zoomLevel != -1) {
	        	googleMap.setZoom(zoomLevel);	
	        }
	        
		},

		/* --------------------------------------------------------------------------- */
		
		"mapEvent" : function (clickIndex, place_id) {
			var that = this;

			this.itemJump(clickIndex);
			this.markerChange(place_id);

			// is there an entry open
			if ( !this.isOpen ) {
				//no
				this.openEntry(clickIndex);
			} else {
				// yes 
				// toggle state of selected entry
				if ( this.openEntryIndex != -1 ) {
					if ( this.openEntryIndex != clickIndex ) {
						// no, close open entry and open selected entry
						this.closeEntry();
						this.stopBounce();
						this.openEntry(clickIndex);
						this.markerChange(place_id);
					} else {
						this.closeEntry();
						this.stopBounce();
					}
				}
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"entryEvent" : function (entry) {
			var that = this,
				clickIndex = this.list.find('li.entry').not('li.template').index(entry);

			// console.log("HertzPlacesSidebar.entryEvent: %s", clickIndex);

			// is there an entry open
			if ( !this.isOpen ) {
				// no
				this.openEntry(clickIndex);
				var pause = window.setTimeout(function(){
					that.mapJump(clickIndex);	
				}, 300);
				
			} else {
				// yes
				// did the uswer click on the open entry?
				if ( this.openEntryIndex == clickIndex ) {
					// yes, close open entry
					this.closeEntry();
					this.stopBounce();
				} else {
					// no, close open entry and open selected entry
					this.closeEntry();
					this.stopBounce();
					this.openEntry(clickIndex);
					var pause = window.setTimeout(function(){
						that.mapJump(clickIndex);	
					}, 300);
				}
			}
		},

		/* --------------------------------------------------------------------------- */
		
		"closeEvent" : function () {
			var that = this;

			this.closeEntry();
			this.stopBounce();
		},

		/* --------------------------------------------------------------------------- */
		
		"markerChange" : function (poiPlaceId) {
			var that = this;

	        if ( this.bounceMarkerIndex == -1 ) {
	        	// no map markers currently bouncing
	        	// start bounce animation
	        	this.startBounce(poiPlaceId);
	        } else {
	        	// stop current bounce
	        	this.stopBounce();
	        	// is the current bounce on the selected poi marker?
	        	if ( poiPlaceId != this.bounceMarkerId ) {
	        		// no, start a new bounce
	        		this.startBounce(poiPlaceId);
	        	}	
	        }
	    },

		/* --------------------------------------------------------------------------- */
		
		"stopBounce" : function () {
			var that = this;

        	var bouncingMarker = poiMarkers[this.bounceMarkerIndex];
        	// bouncingMarker.setAnimation(null);
			bouncingMarker.setZIndex(20);
			bouncingMarker.icon = mapIcons[bouncingMarker.icontype];
        	this.bounceMarkerIndex= -1;
        	this.bounceMarkerId = '';
		},

		/* --------------------------------------------------------------------------- */
		
		"startBounce" : function (poiPlaceId) {
			var that = this;

			var poiIndex = 0, poiCount = poiMarkers.length, tpoi1, tpoi2 = null;

			do {
				tpoi1 = poiMarkerIndex[poiIndex];

				if ( tpoi1.place_id == poiPlaceId) {
					tpoi2 = poiMarkers[tpoi1.index];
					break;
				}
				poiIndex++;
			} while ( poiIndex < poiCount )

			if (typeof tpoi2 == 'object' ) {

				tpoi2.setZIndex(100);
				// tpoi2.setAnimation(google.maps.Animation.BOUNCE);
				tpoi2.icon = mapIcons[tpoi2.icontype + '_large'];
				that.bounceMarkerIndex = tpoi1.index;
				that.bounceMarkerId = tpoi1.place_id;
			}
			/*
			poiMarkers.map(function(poiMarker,index){
				if ( poiPlaceId == poiMarker.place_id ) {
					console.log("marker found index: %s name: '%s' id: %s", index, poiMarker.place_name, poiMarker.place_id);
					poiMarker.setAnimation(google.maps.Animation.BOUNCE);
					that.bounceMarkerIndex = index;
					that.bounceMarkerId = poiPlaceId;
				}
			});
			*/

		},

		/* --------------------------------------------------------------------------- */
		
		"pageImageLoad" : function (page,enigma) {
			var that = this;

			var itemNodes = this.list.find('li.entry').not('li.template'),
				thisNode,
				itemoffset,
				items = [],
				photo,
				cb = new Date();

			itemoffset = (page * 3);
			items.push(itemoffset);
			items.push(itemoffset + 1);
			items.push(itemoffset + 2);

			// iterate through POI items in page
			items.map(function(item,index){
				thisNode = itemNodes.eq(item);

				that.imageLoaders.push(new PoiImageLoader({
					"item" : thisNode
				}));

			});
		},

		/* --------------------------------------------------------------------------- */
		
		"clear" : function () {
			var that = this;

			this.list.find('li.entry').not('li.template').remove();
			this.list.css({"top" : "0px"});

			this.items = [];
			this.entries;
			this.count = 0;

			this.pages = 0;
			this.currentPage = 0;
			this.listOffset = 0;

			this.isOpen = false;
			this.openEntryIndex = -1;

			that.imageLoaders = [];

			// this.upButton.hide();
			// this.downButton.hide();
			$('.results').addClass('noarrows');
		}

		/* --------------------------------------------------------------------------- */
	}

	window.HertzPlacesSidebar = HertzPlacesSidebar;

})();