(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzPlacesSidebarItem
	/* ------------------------------------------------ */
	
	function HertzPlacesSidebarItem(parameters) {
		this.parameters = parameters;
		this.sidebar = parameters.sidebar;
		this.list = parameters.list;
		this.item = parameters.item;

		this.group;
		this.isOpen = false;
		this.openIndex = -1;

		this.init();
	}
	
	HertzPlacesSidebarItem.prototype = {
		"constructor" : HertzPlacesSidebarItem,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			this.build();
		},

		/* --------------------------------------------------------------------------- */
		
		"build" : function () {
			var that = this,
				typeTitle,
				typeIcon,
				newPOINode,
				photoURL1,
				photoURL1x,
				photoURL1a,
				photoURL1b,
				photoURL1c,
				photoURL2;

			this.group = this.item.group;

			// set icon
	        typeTitle = sidebarIcons[this.group].title;
	        typeIcon = sidebarIcons[this.group].img;

		    sidebarIcons
		    newPOINode = this.list.find('li.template').clone().removeClass('template');

		    // set background-image
		    if (this.item.photos) {
		      if (this.item.photos.length > 0 ) {
		      	// plaes search results photo property -> photos[0].getUrl()
		      	photoURL1 = this.item.photos[0].getUrl({ "maxWidth" : 512 }); // curently 5/6/2017 returns invalid URL

		      	newPOINode
		      	.attr({"data-photo" : photoURL1});
		      }
		    }

		    // set main data and bind events
		    newPOINode.addClass(typeTitle);
		    newPOINode
		    .attr({
				"data-place-sd" : Math.floor(this.item.sd), // distance from placesService.nearbySearch centre in metres
				"data-place-id" : this.item.place_id,
				"data-rating" : this.item.rating,
				"data-lat" : this.item.geometry.location.lat(),
				"data-lng" : this.item.geometry.location.lng(),
				"data-distance" : (this.item.distance / 1000) // convert to kilometers
		    });

		    var distanceString,km,m;

		    if ( this.item.distance > 1000 ) {
		    	km = Math.floor(this.item.distance / 1000);
		    	m = Math.floor(this.item.distance - (km * 1000));
		    	// distanceString = km + '.' + m + 'km';
		    	distanceString = km + 'km'; // displaydistance from start of route rounded to nearest kilometre
		    } else {
		    	distanceString = Math.floor(this.item.distance) + 'm';
		    }

		    // set text
		    newPOINode.find('a.opener').on('click', function(e){
		      e.preventDefault();

		      that.sidebar.entryEvent(jQuery(this).parent('li'));

		    });
		    newPOINode.find('a.closer').on('click', function(e){
		      e.preventDefault();

		      that.sidebar.closeEvent();

		    });
		    newPOINode.find('.main h2').text(this.item.name);
		    newPOINode.find('.main img.icon').attr({"src" : typeIcon, "alt" : typeTitle});

		    if ( this.item.vicinity && this.item.vicinity != '' ) {
		    	newPOINode.find('.main address').eq(0).text(this.item.vicinity);	
		    } else {
		    	newPOINode.find('.main address').eq(0).hide();
		    }

		    newPOINode.find('.infoOuter p.distance span').text(distanceString);

		    // add to list container
		    this.list.append(newPOINode);

		}

		/* --------------------------------------------------------------------------- */
	}

	window.HertzPlacesSidebarItem = HertzPlacesSidebarItem;

})();