(function () {
	"use strict";

	/* ------------------------------------------------ */
	// HertzPlaces
	/* ------------------------------------------------ */
	
	function HertzPlaces(parameters) {
		this.parameters = parameters;

		this.items = [];
		this.count = 0;

		this.init();
	}
	
	HertzPlaces.prototype = {
		"constructor" : HertzPlaces,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

		},

		/* --------------------------------------------------------------------------- */

		"addAllPlaces" : function (places) {
			var that = this;

			this.items = places;
			this.count = this.items.length;
		},

		/* --------------------------------------------------------------------------- */

		"addPlace" : function (place) {
			var that = this;
			
			if ( !this.isPlaceInList(place) ) {
				this.items.push(place);
				this.count = this.items.length;
			}
		},

		/* --------------------------------------------------------------------------- */

		"getPlaces" : function () {
			var that = this;
			return this.items;
		},

		/* --------------------------------------------------------------------------- */

		"getCount" : function () {
			var that = this;
			return this.count;
		},

		/* --------------------------------------------------------------------------- */

		"empty" : function () {
			var that = this;
			
			this.items = [];
			this.count = 0;
		},

		/* --------------------------------------------------------------------------- */

		"isPlaceInList" : function (place) {
			var that = this,
				placeIndex = 0,
				thisPlace;

			if ( this.count > 0) {
				do {
					thisPlace = this.items[ placeIndex ];			
					if (place.place_id === thisPlace.place_id ) {
						return true;
					}

					placeIndex++;
				} while ( placeIndex < this.count )
			}

			return false;
		},

		/* --------------------------------------------------------------------------- */

		"typeFilter" : function (requiredTypes) {
			var that = this,
				foundTypes = {},
				workingPOI = [];

			this.items.map(function (place) {
				if ( requiredTypes.indexOf(place.group) != -1) {

					workingPOI.push(place);

					// build list of categories existing in search results
					if ( !foundTypes.hasOwnProperty(place.group) ) {
						foundTypes[place.group] = 1;
					} else {
						foundTypes[place.group]++;
					}

				}

			});

			this.items = workingPOI;
			this.count = this.items.length;

			return foundTypes;
		},

		/* --------------------------------------------------------------------------- */

		"brandFilter" : function (parameters) {
			var that = this,
				workingPOI = [],
				regexString = '',
				testRegEx,
				result,
				badPlaceIndices = [];


			// arrange list so that low prestige brands 'subway', 'macdonalds' get moved to end of list
			// 'Subway', 'Pizza Hut', 'Pizza Express', 'Nando'
			// /Subway|Pizza Hut|Pizza Express|Nando/gi

			/*
			parameters.requiredTypes = ['restaraunt','spa','park']
			parameters.filters = [
				{
					"type" : "restaurant",
					"brands" : ['Subway','MacDonalds','Pizza Express','Pizza Hut']
				}
			]
			*/


			parameters.filters.map(function(filter) {

				if ( parameters.requiredTypes.indexOf(filter.type) != -1 || filter.type == 'global' ) {
				// if ( filter.type == 'global' ) {
					// filter type exists in required types, check place name against list of brands/names

					// build regex from list of brands
					filter.brands.map(function(brand){
						regexString = (regexString != '') ? regexString + '|' : regexString;
						regexString = regexString + brand
					});
					
					testRegEx = new RegExp(regexString, 'i');

					// iterate through places to see if the place name matches one of the proscribed brands
					that.items.map(function(place, placeIndex){
						result = testRegEx.exec(place.name);
						if(result != null){
							// console.log(result[0]);
							badPlaceIndices.push(placeIndex);
						}

					});
					badPlaceIndices.reverse(0); // reverse order

					// remove identified places
					if (badPlaceIndices.length > 0) {
						var rpi = 0,
							rpc = badPlaceIndices.length,
							trp;

						do {
							trp = badPlaceIndices[rpi];
							that.items.splice(trp, 1);

							rpi++;
						} while (rpi < rpc)

						that.count = that.items.length;
					}
				}
			});			
		},

		/* --------------------------------------------------------------------------- */

		"ratingSort" : function () {
			var that = this;

			// sort items by rating 

			// this.items = this.quickSort(this.items, 0, this.count -1);
		    this.items.sort(function (a, b)
		    {
		        return b.rating - a.rating; // descending order based on rating property
		    });
		}, 

		/* --------------------------------------------------------------------------- */

		"quickSort" : function  (arr, left, right) {
			var i = left,
				j = right,
				tmp,
				pivotidx = (left + right) / 2,
				pivot = arr[pivotidx.toFixed()].rating;  

			/* partition */
			while (i <= j)
			{
				// ascending order
				/*
				while (arr[i].rating < pivot) {
					i++;
				}

				while (arr[j].rating > pivot) {
					j--;
				}
				*/

				// descending order
				while (arr[i].rating > pivot) {
					i++;
				}

				while (arr[j].rating < pivot) {
					j--;
				}
				if (i <= j)
				{
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;

					i++;
					j--;
				}
			}

			/* recursion */
			if (left < j)
				this.quickSort(arr, left, j);
			if (i < right)
				this.quickSort(arr, i, right);
			return arr;
		}

		/* --------------------------------------------------------------------------- */	
	}

  
	window.HertzPlaces = HertzPlaces;

})();