	// vivaldi
	// specify width and height attributes in svg 
	// <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 125" style="enable-background:new 0 0 100 125;" width="100px" height="125px" xml:space="preserve">

	// firefox 
	// specify width and height in svg,
	// specify color declaration using rgb(R,G,B) not #RRGGBB as firefox chokes on '#' characters in svg files
	// specify "size" property in icon declaration

	// set up default icon
	var mapIcons = [],
		anchor = {
			"normal" : [22,50],
			"large" : [32,75]
		},
		size = [100, 125],
		scaledSize = {
			"normal" : [43,59],
			"large" : [65,88]
		};

	function buildIcons(type) {
		var iconDefs = [];

		iconList.map(function(icon){
			var tempIcon = {},
				activeType = icon[type];

			tempIcon.url = activeType.url;

			if (activeType.size.length > 0 ) {
				tempIcon.size = new google.maps.Size(activeType.size[0],activeType.size[1]);
			}

			if (activeType.scaled.length > 0 ) {
				tempIcon.scaledSize = new google.maps.Size(activeType.scaled[0],activeType.scaled[1]);
			}

			if (activeType.anchor.length > 0 ) {
				tempIcon.anchor = new google.maps.Point(activeType.anchor[0],activeType.anchor[1]);
			}
			
			iconDefs[icon.key] = tempIcon;
		});

		return iconDefs;
	}

	var iconList = [
		{
			"key" : "default",
			"svg" : {
				"url" : "assets/svg/white-google-map-pin.svg",
				"size" : [],
				"anchor" : [17,50],
				"scaled" : [60,60]
			},
			"png" : {
				"url" : "",
				"size" : [],
				"anchor" : [],
				"scaled" : [60,60]
			}
		},
		{
			"key" : "start",
			"svg" : {
				"url" : "assets/svg/green-google-map-pin.svg",
				"size" : [],
				"anchor" : [17,50],
				"scaled" : [60,60]
			},
			"png" : {
				"url" : "assets/img/icons/map/start.png",
				"size" : [56,70],
				"anchor" : [16,40],
				"scaled" : [32,40]
			}
		},
		{
			"key" : "end",
			"svg" : {
				"url" : "assets/svg/red-google-map-pin.svg",
				"size" : [],
				"anchor" : [17,50],
				"scaled" : [60,60]
			},
			"png" : {
				"url" : "assets/img/icons/map/end.png",
				"size" : [56,70],
				"anchor" : [16,40],
				"scaled" : [32,40]
			}
		},
		{
			"key" : "art_gallery",
			"svg" : {
				"url" : "assets/svg/map/art_gallery.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/gallery.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "art_gallery_large",
			"svg" : {
				"url" : "assets/svg/map/art_gallery.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/gallery.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "museum",
			"svg" : {
				"url" : "assets/svg/map/museum.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/museum.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "museum_large",
			"svg" : {
				"url" : "assets/svg/map/museum.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/museum.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "restaurant",
			"svg" : {
				"url" : "assets/svg/map/restaurant.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/restaurant.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "restaurant_large",
			"svg" : {
				"url" : "assets/svg/map/restaurant.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/restaurant.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "spa",
			"svg" : {
				"url" : "assets/svg/map/spa.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/spa.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "spa_large",
			"svg" : {
				"url" : "assets/svg/map/spa.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/spa.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "stadium",
			"svg" : {
				"url" : "assets/svg/map/stadium.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/stadium.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "stadium_large",
			"svg" : {
				"url" : "assets/svg/map/stadium.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/stadium.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "park",
			"svg" : {
				"url" : "assets/svg/map/park.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/park.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "park_large",
			"svg" : {
				"url" : "assets/svg/map/park.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/park.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "zoo",
			"svg" : {
				"url" : "assets/svg/map/zoo.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/zoo.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "zoo_large",
			"svg" : {
				"url" : "assets/svg/map/zoo.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/zoo.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "amusement_park",
			"svg" : {
				"url" : "assets/svg/map/amusement_park.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/amusement_park.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "amusement_park_large",
			"svg" : {
				"url" : "assets/svg/map/amusement_park.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/amusement_park.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "bakery",
			"svg" : {
				"url" : "assets/svg/map/bakery.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/bakery.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "bakery_large",
			"svg" : {
				"url" : "assets/svg/map/bakery.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/bakery.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		},
		{
			"key" : "cafe",
			"svg" : {
				"url" : "assets/svg/map/cafe.svg",
				"size" : size,
				"anchor" : anchor.normal,
				"scaled" : scaledSize.normal
			},
			"png" : {
				"url" : "assets/img/icons/map/cafe.png",
				"size" : [27,38],
				"anchor" : [13,38],
				"scaled" : [27,38]
			}
		},
		{
			"key" : "cafe_large",
			"svg" : {
				"url" : "assets/svg/map/cafe.svg",
				"size" : size,
				"anchor" : anchor.large,
				"scaled" : scaledSize.large
			},
			"png" : {
				"url" : "assets/img/icons/map/cafe.png",
				"size" : [43,59],
				"anchor" : [22,59],
				"scaled" : [43,59]
			}
		}
	];

	var sidebarIcons = [];

	sidebarIcons['art_gallery'] = {
		"title" : "gallery",
		"img": "assets/img/icons/sidebar/gallery.png"
	};

	sidebarIcons['museum'] = {
		"title" : "museum",
		"img": "assets/img/icons/sidebar/museum.png"
	};

	sidebarIcons['restaurant'] = {
		"title" : "restaurant",
		"img": "assets/img/icons/sidebar/restaurant.png"
	};
	sidebarIcons['spa'] = {
		"title" : "spa",
		"img": "assets/img/icons/sidebar/spa.png"
	};
	sidebarIcons['stadium'] = {
		"title" : "stadium",
		"img": "assets/img/icons/sidebar/stadium.png"
	};
	sidebarIcons['amusement_park'] = {
		"title" : "amusement park",
		"img": "assets/img/icons/sidebar/amusement_park.png"
	};
	sidebarIcons['park'] = {
		"title" : "park",
		"img": "assets/img/icons/sidebar/park.png"
	};
	sidebarIcons['zoo'] = {
		"title" : "zoo",
		"img": "assets/img/icons/sidebar/zoo.png"
	};
	sidebarIcons['bakery'] = {
		"title" : "bakery",
		"img": "assets/img/icons/sidebar/bakery.png"
	};
	sidebarIcons['cafe'] = {
		"title" : "cafe",
		"img": "assets/img/icons/sidebar/cafe.png"
	};
