var supportsCSSTransitions, supportsCSSAnimation, supportsGelocation, supportsSVG, supportsSVGforeignObject, isIE, pngTest, ishighdpi, expanderOpen = false, mainStateController;
var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

var googleMap,
	mapOptions = {
		// center: mapCenterLocation,
		// mapTypeId: "BW",
		// mapTypeControlOptions: {mapTypeIds: ["BW"]},
		"mapTypeId" : "hybrid",
		"zoom" : 12,
	},
	filterContext = 'form',
	currentFilter = '',
	HertzYellow = "#ffcf2c",
	radii = [],
	markers = [],
	poiMarkers = [],
	poiMarkerIndex = [],
	lines = [],
	boxPolys = [],
	placesService,
	windowSize,
	zoomLevel = -1, // HertzPlacesSidebar.mapJump, HertzStateController.buildPOIMarker, -1 turns off zoom change
	TESTMODE = false;

	/* Forli to Milan thanks katerina sforza */
	var forli = {
		"label" : "Forli, Province of Forli-Cesena, Italy",
		"lat" : 44.2227398,
		"lng" : 12.040731199999982
	};
	var ovierto = {
		"label" : "05018 Orvieto, Province of Terni, Italy",
		"lat" : 42.7185068,
		"lng" : 12.110744599999975
	};

(function () {
	"use strict";

	Number.prototype.toRad = function () {
	  return this * Math.PI / 180;
	};

	Number.prototype.toDeg = function () {
	  return this * 180 / Math.PI;
	};

	Number.prototype.toBrng = function () {
	  return (this.toDeg() + 360) % 360;
	};

	window.requestAnimFrame = (function(callback) {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
	})();

	if (!window.console) {
		window.console = {
			"log" : function(){}
		}
	}

	/* ---------------------------------------------------------------------------------- */
	// document ready START
	/* ---------------------------------------------------------------------------------- */

	/* */
	/*
	window.onload = function(e){};
	*/
	/* */	

	$(document).ready(function () {

		supportsCSSAnimation = $('html').hasClass('cssanimations');
		supportsCSSTransitions = $('html').hasClass('csstransitions');
		supportsGelocation = $('html').hasClass('geolocation');
		supportsSVG = $('html').hasClass('svg');
		supportsSVGforeignObject = $('html').hasClass('svgforeignobject');
		isIE = $('html').hasClass('ie') || isIE11;

		/*
		windowSize = {
			"w" : $(window).width(),
			"h" : $(window).height()
		};

		$(window).resize(function(){
			var throttle = window.setTimeout(function(){
				windowSize = {
					"w" : $(window).width(),
					"h" : $(window).height()
				};
				//console.log("w: %s h: %s", windowSize.w, windowSize.h );
			}, 500);
		});
		*/
		ishighdpi = ( window.devicePixelRatio > 1);

		// browser geolocation API support
		// http://caniuse.com/#feat=geolocation

		if ( supportsGelocation ) {
			/* geolocation is available */
			// console.log('supports geolocation');

			// https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation
			// https://stackoverflow.com/questions/17402103/google-api-for-location-based-on-user-ip-address

			// requires https host
			/*  */
			navigator.geolocation.getCurrentPosition(function(position) {
				console.log(position.coords.latitude, position.coords.longitude);
			});
			/*  */

		} else {
		  /* geolocation IS NOT available */
		  // console.log('no support for geolocation');
		}

		var filterList = $('.filterList');
		$('.button').on('click',function(e){
			e.preventDefault();

			if( $(this).hasClass('open') ) {
				// open button
				if( !expanderOpen ) {
					filterList.addClass('open');
					expanderOpen = true;
				} else {
					filterList.removeClass('open');
					expanderOpen = false;
				}
			} else {
				// close button
				if( expanderOpen ) {
					filterList.removeClass('open');
					expanderOpen = false;
				}
			}
		});

		// close radio button filter when user selects a radio button
		$('.filterList li').on('click',function(e){
			e.preventDefault();

			var filterList = $('.filterList'),
				newCaption = $(this).text();
				currentFilter = $(this).attr('data-value');

			if ( !$(this).hasClass('checked') ) {

				$('.filterList li.checked').removeClass('checked');

				$(this).addClass('checked');
				
				$('#' + filterContext + ' .filterList a.open.button').html(newCaption);
				filterList.removeClass('zero').addClass('one');
				$('.filterList').removeClass('error');
			} else {
				$('#' + filterContext + ' .filterList a.open.button').html(newCaption);
				filterList.removeClass('zero').addClass('one');
				console.log(filterContext);
				
			}
			filterList.removeClass('open');
			expanderOpen = false;
		});

		// define map label language/autocomplete language by adding language parameter to google map library link
		// <script src="https://maps.googleapis.com/maps/api/js?key=[API KEY]&amp;libraries=places&amp;language=fr"></script>
		// language=fr

		pngTest = (window.location.href.indexOf('test=png') !== -1);
		mapIcons = buildIcons( (!isIE && !pngTest) ? 'svg' : 'png' );
		// mapIcons = buildIcons( 'png' );

		mainStateController = new HertzStateController({
			"language" : mainLanguage,
			"pages" : ["form","map"],
			"formPage" : $('#form'),
			"formStartField" : "f_startPoint",
			"formEndField" : "f_endPoint",
			"formGoButton" : $('#form a.go'),
			"mapPage" : $('#map'),
			"mapStartField" : "startPoint",
			"mapEndField" : "endPoint",
			"mapGoButton" : $('#map a.go'),
			"mapResetButton" : $('#map a.reset'),
			"mapContainer" : $('#map .map'),
			"mapNode" : "mapbox",
			"sidebarContainer" : $('.results'),
			"sidebar" : $('#poilist'),
			"defaultTypes" : ['art_gallery', 'museum', 'restaurant', 'spa', 'stadium', 'zoo', 'amusement_park', 'park', 'bakery'],
			"brandFilters" : [
				{
					"type" : "global",
					"brands" :["Holiday Inn", "Premier Inn", "Novotel", "Mercure Hotel", "Marriott Hotel", "Travelodge", "DaysInn Hotel", "Holiday Park", "Airport", "Butlins ", "Havens Holidays", "Havens Holiday\'s", "Pontins"]
				},
				{
					"type" : "restaurant",
					"brands" : ["McDonalds", "MC Donnalds ", "Mc Donalds", "McDonald\'s", "Quick", "Subway", "KFC", "Kebab", "Burger King", "Pizza Hut", "Big Fernand", "Nabab", "Paul", "Flunch", "La Mie Caline", "Campaigns", "Wimpy", "Greggs", "Gregg", "Little Chef", "Eat", "Morley\'s Chicken", "Chicken Cottage", "Southern Fried Chicken", "Upper Crust", "West Cornwall Pasty Company", "West Cornwall Pasty Co", "Five Guys", "Costa Coffee", "Café Nero", "Cafe Nero", "Starbucks", "Pret", "Sam\'s Chicken", "Millie\'s Cookies", "Dixy Chicken", "Weatherspoon\'s", "Kebabs", "Nordsee", "Pizza Hut,", "Vapiano", "Doener Kebab", "Doner Kebab", "Sausage shops", "Spizzico", "Kebabs", "Ciao", "Autogrill", "Flunch", "Burger Ranch", "Domino\'s Pizza", "Domincos Pizza", "Wendy\'s", "Papa John\'s Pizza", "Pelicana Chicken", "Tasty Chicken", "Southern Fried Chicken", "Febo", "Panos", "Taco Bell", "Chipstart", "Casa de Kebab", "Patatas Queen", "Takeaway", ]
				},
				{
					"type" : "park",
					"brands" : ['campsite', 'camp site','camping','holiday', 'town hall', 'townhall', 'hotel', 'car park']
				},
				{
					"type" : "bakery",
					"brands" : ['Greggs', 'Gregg\'s', "West Cornwall Pasty Company", "West Cornwall Pasty Co", "West Cornwall Pasty"]
				}
			],
			"searchWindowStartPercent" : 35,
			"searchWindowEndPercent" : 65,
			"searchSegments" : 12,
			"mapIcons" : mapIcons,
			"sidebarIcons" : sidebarIcons,
			"geolocationEnabled" : supportsGelocation,
			"geoPosition" : { "lat" : 0, "lng" : 0 }
		});
	});

	/* ---------------------------------------------------------------------------------- */
	// document ready END
	/* ---------------------------------------------------------------------------------- */

})();